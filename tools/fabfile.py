


import sys
import os
from datetime import datetime
import shutil

TEMPLATE = """
{title}
{hashes}

:date: {year}-{month}-{day} {hour}:{minute:02d}
:tags: {tags}
:category: {category}
:slug: {slug}


"""

# TEMPLATE is declared before hand, and all the necessary imports made
def make_entry_rst(title="My first title",tags="python",category="programming"):
    today = datetime.today()
    slug = title.lower().strip().replace(' ', '-')
    f_create = "content/{}_{:0>2}_{:0>2}_{}.rst".format(
        today.year, today.month, today.day, slug)
    t = TEMPLATE.strip().format(title=title,
                                hashes='#' * len(title),
                                tags=tags,
                                category=category,
                                year=today.year,
                                month=today.month,
                                day=today.day,
                                hour=today.hour,
                                minute=today.minute,
                                slug=slug)
    with open(f_create, 'w') as w:
        w.write(t)
    print("File created -> " + f_create)
    
    
    
    

TEMPLATE_ipynb = """
Title: {title}
Slug: {slug}
Date: {year}-{month}-{day} {hour}:{minute:02d}
Category: {category}
Tags: {tags}
Author: {author}


"""


def make_entry_ipython(notebook="./test.ipynb",title="My first title",tags="python",category="programming",author="Me"):
    today = datetime.today()
    slug = title.lower().strip().replace(' ', '-')
    shutil.copy(notebook,"content/{}_{:0>2}_{:0>2}_{}".format(
     today.year, today.month, today.day,os.path.basename(notebook))
    )
    f_create = "content/{}_{:0>2}_{:0>2}_{}-meta".format(
    today.year, today.month, today.day,os.path.basename(notebook))    
    t = TEMPLATE_ipynb.strip().format(title=title,
                                tags=tags,
                                category=category,
                                year=today.year,
                                month=today.month,
                                day=today.day,
                                hour=today.hour,
                                minute=today.minute,
                                slug=slug,
                                author=author
    )
    with open(f_create, 'w') as w:
        w.write(t)
    print("File created -> " + f_create)
    
