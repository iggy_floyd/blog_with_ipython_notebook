# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project

# to use bash shell in Makefile
SHELL =	  /bin/bash  

# settings of the blog: a source folder, a title, an author 
BLOG_SOURCE = src
BLOG_TITLE = "Programmatic\ Statistics"
BLOG_AUTHOR = "Dr.\ Igor\ Marfin"

# settings of the github page to host the blog
GITREPOPATH     := https://api.github.com/user/repos 
GITREPO         := igormarfin.github.io
GITUSER         := igormarfin


all: configuration doc 


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac


blog_ini:
	-@ echo "Initialization of the blog ..."
	-@ [[ ! `cat .gitignore | grep $(BLOG_SOURCE)` ]] && echo "$(BLOG_SOURCE)/">> .gitignore
	-@ echo " Before you continue, you can look at the variant of answers you could be propsed..."; cat pelican-quickstart.example;
	-@ pelican-quickstart -p $(BLOG_SOURCE) -a "$(BLOG_AUTHOR)" -t "$(BLOG_TITLE)"
	-@ cat tools/fabfile.py >>$(BLOG_SOURCE)/fabfile.py
	-@ cp -r plugins $(BLOG_SOURCE)
	-@ echo "PLUGIN_PATHS = ['./plugins'] " >> $(BLOG_SOURCE)/pelicanconf.py	
	-@ echo "MARKUP = ('md', 'ipynb') " >> $(BLOG_SOURCE)/pelicanconf.py
	-@ echo "PLUGINS = ['ipynb','share_post','tipue_search','tag_cloud'] " >> $(BLOG_SOURCE)/pelicanconf.py
	-@ echo >> $(BLOG_SOURCE)/pelicanconf.py
	-@ cat mypelicanconfig.py >> $(BLOG_SOURCE)/pelicanconf.py
	-@ cp -r theme $(BLOG_SOURCE)/
	-@ echo " " >> $(BLOG_SOURCE)/pelicanconf.py
#	-@ echo "import os" >> $(BLOG_SOURCE)/pelicanconf.py
#	-@ echo "path = os.path.curdir" >> $(BLOG_SOURCE)/pelicanconf.py
#	-@ echo "THEME = '%s/theme' % path" >> $(BLOG_SOURCE)/pelicanconf.py
#	-@ cp $(BLOG_SOURCE)/pelicanconf.py $(BLOG_SOURCE)/publishconf.py
	-@ cat publishconf.py.template >> $(BLOG_SOURCE)/publishconf.py
	-@ make add_entry_rst HOW-TO make this blog,create pelican blog,BuildingThisBlog 
	-@ cat how-to-make-this-blog.rst >> $(BLOG_SOURCE)/content/*how-to-make-this-blog.rst


post_how_to_make_this_blog:
	-@ make add_entry_rst HOW-TO make this blog,create pelican blog,BuildingThisBlog 
	-@ cat how-to-make-this-blog.rst >> $(BLOG_SOURCE)/content/*how-to-make-this-blog.rst

post_about_me:
	-@ make add_entry_rst About Me,about me,About me 
	-@ cat about-me.rst >> $(BLOG_SOURCE)/content/*about-me.rst

post_with_image:
	-@ make add_entry_rst A post with an Image,blog\\\,post\\\,image,BuildingThisBlog
	-@ cat how-to-post-with-image.rst >> $(BLOG_SOURCE)/content/*a-post-with-an-image.rst
	-@ mkdir -p $(BLOG_SOURCE)/content/images
	-@ cp pelican-plugin-post-stats-medium-example.png $(BLOG_SOURCE)/content/images/



# If the first argument is "add_entry_rst"...
ifeq (add_entry_rst,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

add_entry_rst:
	-@ cd $(BLOG_SOURCE); fab make_entry_rst:"$(RUN_ARGS)" 



# If the first argument is "add_entry_ipython"...
ifeq (add_entry_ipython,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  IPYTHON_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(IPYTHON_ARGS):;@:)
endif

add_entry_ipython:
	-@ cd $(BLOG_SOURCE); fab make_entry_ipython:"$(IPYTHON_ARGS),$(BLOG_AUTHOR)" 


# If the first argument is "remove_entry"...
ifeq (remove_entry,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  REMOVE_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(REMOVE_ARGS):;@:)
endif

remove_entry:
	-@ cd $(BLOG_SOURCE); find ./ -iname "*$(REMOVE_ARGS)*" -exec rm -r {} \; 
	-@ cd $(BLOG_SOURCE); find ./ -iname `echo "*$(REMOVE_ARGS)*" | tr -d '-'` -exec rm -r {} \; 

# If the first argument is "add_directories"...
ifeq (add_directories,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  ADDDIRS_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(ADDDIRS_ARGS):;@:)
endif

add_directories:
	-@ cd $(BLOG_SOURCE); find ./ -iname "*$(ADDDIRS_ARGS)*" -exec cp -r  $(SOURCE_DIR) {} \; 


html:
	-@ cd $(BLOG_SOURCE);  make html;

html_publish:
	-@ cd $(BLOG_SOURCE);  make  publish;

	
	
add_plugins:
	-@ cp -r plugins $(BLOG_SOURCE)
	-@ echo "PLUGIN_PATHS = ['./plugins'] " >> $(BLOG_SOURCE)/pelicanconf.py
	-@ echo "PLUGIN_PATHS = ['./plugins'] " >> $(BLOG_SOURCE)/publishconf.py

add_ipynb_plugin:
	-@ echo "MARKUP = ('md', 'ipynb') " >> $(BLOG_SOURCE)/publishconf.py
	-@ echo "MARKUP = ('md', 'ipynb') " >> $(BLOG_SOURCE)/pelicanconf.py
	-@ echo "PLUGINS = ['ipynb'] " >> $(BLOG_SOURCE)/publishconf.py
	-@ echo "PLUGINS = ['ipynb'] " >> $(BLOG_SOURCE)/pelicanconf.py



doc: README.wiki 

	-@ mkdir doc 2>/dev/null 1>/dev/null
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc/{}.html;  rm -r {}" | sh


run: configuration 

	-@ echo "Not implemented"

 
GitHubPages_ini: 
	curl -u '$(GITUSER)' $(GITREPOPATH)  -d '{"name":"$(GITREPO)"}' 

fetch_blog:
	-@ git clone  https://github.com/$(GITUSER)/$(GITREPO).git $(BLOG_SOURCE);
	-@ cd $(BLOG_SOURCE); \
	git checkout source;

publish:
	-@ cd $(BLOG_SOURCE); \
	git init; \
	git add . ;\
	git commit -am "new posts deploying";\
	git remote add origin https://github.com/$(GITUSER)/$(GITREPO).git; \
	git branch source; \
	git checkout source; \
	rm output/output; \
	git fetch origin source; \
	git merge -X ours -m "new posts deploying" source; \
	git commit -am "new posts deploying"; \
	git branch gh-pages;  \
	ghp-import output; \
	git checkout gh-pages; \
	git fetch origin gh-pages; \
	git merge -X ours -m "new posts deploying" gh-pages; \
	git commit -am "new posts deploying";\
	git checkout master; \
	git fetch origin master; \
	git merge -X ours -m "new posts deploying" master; \
	git commit -am "new posts deploying";\
	git merge gh-pages  -m "new posts deploying"; \
	git push --all --force; \
	git checkout source;

clone: 
	-@ git clone https://github.com/$(GITUSER)/$(GITREPO).git $(BLOG_SOURCE)
	-@ cd $(BLOG_SOURCE); \
	git checkout source;


add_comments_support:
	-@ echo  >> $(BLOG_SOURCE)/publishconf.py
	-@ echo  >> $(BLOG_SOURCE)/publishconf.py
	-@ echo "SITEURL = \"http://$(GITREPO)/\"" >> $(BLOG_SOURCE)/publishconf.py
	-@ echo  >> $(BLOG_SOURCE)/publishconf.py
	-@ echo "# DISQUS comments" >> $(BLOG_SOURCE)/publishconf.py
	-@ echo "DISQUS_SITENAME = \"$(DISQUS_SITE)\"" >> $(BLOG_SOURCE)/publishconf.py

# to clean all temporary stuff
clean: 
	-@rm -r config.log autom4te.cache
	-@rm -rf doc $(BLOG_SOURCE)


serve:
	-@firefox localhost:8010 & 
	-@ln -s $(BLOG_SOURCE)/output/ public
	-@pushd public/;    python -m SimpleHTTPServer 8010; popd;

.PHONY: configuration clean all doc run  serve blog_ini add_entry_rst html GitHubPages_ini publish add_plugins add_ipynb_plugin clone remove_entry add_comments_support fetch_blog post_how_to_make_this_blog post_about_me post_with_image
