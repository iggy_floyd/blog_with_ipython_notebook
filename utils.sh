
_gui_init()
{

#_gui_init_begin_


local help='
ini of gui. There are 4 additional functions:

_menu
_radio_list
_check_list
_gui_main ( example of simple gui application )

which supply GUI : via dialog,whiptail or simple terminal

#$1 -- prefered choice, ie "whiptail"
how to run: 
_gui_init "dialog"
_gui_init "whiptail"
_gui_init "terminal"

'

[ "$1" = "help" -o $# -gt 1 -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1;

[[ -n "$1"   ]] && _prog="$1"


which dialog &> /dev/null;
retval=$?;

[ $retval -eq 0 -a -z "$_prog" ]  &&   _prog=dialog;

which whiptail &> /dev/null;
retval=$?;

[ $retval -eq 0 -a -z "$_prog" ]  &&  _prog=whiptail;

[[ "$1" = "terminal"   ]] && _prog=""


export tempfile1=./dialog_1_$$
export tempfile2=./dialog_2_$$
export tempfile3=./dialog_3_$$
export tempfile4=./dialog_4_$$
export tempfile5=./dialog_5_$$
export tempfile6=./dialog_6_$$

export _prog

trap "rm -f $tempfile1 $tempfile2 $tempfile3" 0 1 2 5 15

#_gui_init_end_
}

_menu()
{
#_menu_begin_


local help='
GUI:

_menu

which supplies  GUI menu : via dialog,whiptail or simple terminal

can be started as _menu "\"my title\"" "\"my menu\""  "item1" "item2"
 
'
[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1;
 
#[[ "$1" = "help" -o $# -lt 1 ]] &&  (print_help "$0" "$help"; return 1;)


[[ -f "$tempfile1" ]] && rm -f $tempfile1;

_title="$1"
_question="$2"


_menustr=" ";
_vars=()
for ((i=3;i<=$#; i++));do _var=`eval echo \\$$i` ; _vars[$((i-2))]=$_var; _menustr=`echo $_menustr $((i-2)) "\"""$_var""\""`; done

#echo ${_vars[@]}
[[ -n "$_prog" ]] &&  echo $_prog --clear  --title "$_title" --menu "$_question"  15 65 0  "$_menustr" | sh 2>  $tempfile1 


[[ -z "$_prog" ]] && 
select opt in "${_vars[@]}" ; do
#        echo $REPLY
#        echo "${#_vars[*]}"
        case "$REPLY" in
                [1-"${#_vars[*]}"])  echo $REPLY > $tempfile1; break;;
                $((${#_vars}+1)))  echo "exit" > $tempfile1; break;;

                *) echo "Invalid option. Try another one.";continue;;

        esac
done



#cat  $tempfile1    
#rm -f $tempfile1;

#_menu_end_
}


_radio_list()
{

#_radio_list_begin_

 
local help='
GUI:

_radio_list

which supplies  GUI radiolist : via dialog,whiptail or simple terminal

can be started as _radio_list  "\"my list\""  "item1" "item2"

'

[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1;  
#[[ "$1" = "help" -o $# -lt 1 ]] &&  (print_help "$0" "$help"; return 1;)


[[ -f "$tempfile2" ]] && rm -f $tempfile2;


_title="$1"


_menustr=" ";
_vars=()

for ((i=2;i<=$#; i++));do _var=`eval echo \\$$i` ; _vars[$((i-1))]=$_var; _menustr=`echo $_menustr $((i-1)) "\"""$_var""\"" off`; done

#echo $_prog   --radiolist  "$_title"  15 40  ${#_vars}   "$_menustr"


[[ -n "$_prog" ]] &&  echo $_prog   --radiolist  "$_title"  15 40  ${#_vars}   "$_menustr" | sh 2>  $tempfile2


[[ -z "$_prog" ]] &&
(echo ""; echo "$_title"; echo""; select opt in "${_vars[@]}" ; do
#        echo $REPLY
        case "$REPLY" in
                [1-"${#_vars[*]}"])  echo $REPLY > $tempfile2;  break;;
                $((${#_vars[*]}+1)))  echo "exit" > $tempfile2; break;;
    
                *) echo "Invalid option. Try another one.";continue;;

        esac
done
)  
 

#cat  $tempfile2
#rm -f $tempfile2;

#_radio_list_end_
}


_check_list()
{
#_check_list_begin_


local help='
GUI:

_check_list
 
which supplies  GUI checklist : via dialog,whiptail or simple terminal

can be started as _check_list  "\"my list\""  "item1" "item2"

'
[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1; 
#[[ "$1" = "help" -o $# -lt 1 ]] &&  (print_help "$0" "$help"; return 1;)


[[ -f "$tempfile3" ]] && rm -f $tempfile3;


_title="$1"


_menustr=" ";
_vars=()
for ((i=2;i<=$#; i++));do _var=`eval echo \\$$i` ; _vars[$((i-1))]=$_var; _menustr=`echo $_menustr $((i-1)) "\"""$_var""\"" off`; done


#echo $_prog   --checklist  "$_title"  15 40  ${#_vars}   "$_menustr"


[[ -n "$_prog" ]] &&  echo $_prog   --checklist  "$_title"  15 40  ${#_vars}   "$_menustr" | sh 2>  $tempfile3


[[ -z "$_prog" ]] &&
(touch  $tempfile3;echo ""; echo "$_title"; echo""; 
select opt in "${_vars[@]}" "exit" ; do
#        echo $REPLY
        case "$REPLY" in
                [1-"${#_vars[*]}"])  echo "$REPLY " >> $tempfile3; continue;;
                $((${#_vars[*]}+1)))   break;;
    
                *) echo "Invalid option. Try another one.";continue;;

        esac
done
)  
 

#cat  $tempfile3
#rm -f $tempfile3;

#_check_list_end_
}

_info_box()
{
#_info_box_begin_


local help='
GUI:

_info_box
 
which supplies  GUI checklist : via dialog,whiptail or simple terminal

can be started as _info_box  "\"my info box\""  "file.msg "

'
[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1; 


[[ -f "$tempfile4" ]] && rm -f $tempfile4;


_title="$1"



[[ -n "$_prog" ]] && [[ "$_prog" = dialog ]] &&  echo  $_prog   --title  "$_title"  --textbox  \"$2\" 22 70  | sh
[[ -n "$_prog" ]] && [[ "$_prog" = whiptail ]] &&  echo  $_prog   --title  "$_title" --scrolltext --textbox  \"$2\" 22 70 |sh

[[ -z "$_prog" ]] &&
(touch  $tempfile4;echo " "; echo "\"$_title\""; echo " ";echo "_BEGIN_" ; echo " "     
cat $2 | awk '{print "*         " $0}';
echo ""; echo "_END_"; echo " ";
)  
 

#cat  $tempfile4
#rm -f $tempfile4;

#_info_box_end_
}


_input_box()       
{
#_input_box_begin_

 
local help='
GUI:    

_input_box

which supplies  GUI checklist : via dialog,whiptail or simple terminal

can be started as _input_box  "\"my input box\""  22 70 2> some_file
        
'

[[ -f "$tempfile5" ]] && rm -f $tempfile5;


[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1;

[[ -f tttt ]] && rm tttt;


[[ -n "$_prog" ]]  &&  echo  $_prog   --inputbox  "$_title"    22 70  | sh 2>  $tempfile5 && cat $tempfile5 | tr '\\n' '\n' > tttt &&  mv tttt $tempfile5;





[[ -z "$_prog" ]] &&
(echo " "; echo "\"$_title\""; echo " ";echo "_BEGIN_" ; echo " "
cat - > $tempfile5 
echo " "
echo " _END_ "
echo " "
)


#cat  $tempfile5;
#rm -f $tempfile5;

#_input_box_end_
}

_form()
{
#_form_begin_


local help='
GUI:

_form

which supplies  GUI menu : via dialog,whiptail or simple terminal

can be started as _form "\"my title\"" "\"my form\""  "item1" "item2"
 
'
[ "$1" = "help" -o $# -lt 1 ] &&  print_help "$0" "$help" && return 1;
 
#[[ "$1" = "help" -o $# -lt 1 ]] &&  (print_help "$0" "$help"; return 1;)


[[ -f "$tempfile6" ]] && rm -f $tempfile6;

_title="$1"
_question="$2"
_template="$3"

#echo "prog = $_prog"

_begin=3;
[[ -f  "$_template" ]] && _begin=4;

echo "_begin=$_begin"
_menustr=" ";
_vars=()
for ((i="$_begin";i<="$#"; i++))
do 
_var=`eval echo \\$$i` 
[[ -f  "$_template" ]] && _temp_var=`cat $_template| sed -e '/^#/d' -ne "/$_var/p" | awk -F '|' '{print $2}'`; 

[ -z $_temp_var ] &&  _temp_var="\\\"some value\\\"" ; 

#echo "temp=$_temp_var"
#echo "var=$_var"
_vars[$((i- _begin+1))]=$_var; 
_menustr=`echo $_menustr  "\"""$_var""\""  $((i-_begin+1)) 1 "\"$_temp_var\"" $((i-_begin+1)) 15 300 300`; 
done



#echo $_prog --clear  --title "$_title" --form "$_question"  15 205 10  "$_menustr"

if [ `echo $_prog | grep -c "dialog"` -eq 1 ] 
then
  echo $_prog --clear  --title "$_title" --form "$_question"  15 205 10  "$_menustr" | /bin/bash 2>  $tempfile6 

else 

echo " "; echo "\"$_tite\""; echo " ";echo "_BEGIN_" ; echo " "; touch $tempfile6
for ((i=$_begin;i<=$#; i++))
do

_var=`eval echo \\$$i` 
_temp_var="\\\"some value\\\"" ; 
[[ -f  "$_template" ]] && _temp_var=`cat $_template| sed -ne "/$_var/p" | awk -F '|' '{print $2}'`; 
echo  "\"""$_var""\":  $_temp_var" >>   $tempfile6;
done
pico -t -x  $tempfile6
echo " "
echo " _END_ "
echo " "
for ((i=$_begin;i<=$#; i++)); do _var=`eval echo \\$$i`; echo $i; echo "$_var"; cat $tempfile6 | sed -ne "s/\"$_var\"\://g"  -ne 's/^[ \t]*//' -e 'p' > tttt; cat tttt; mv tttt $tempfile6; done
fi



#_form_end_
}


_gui_main()
{
#_gui_main_begin_

while true
do
 
        _menu "\" title\"" "\"menu\"" "item 1" "item 2" "input" "exit"

        local retval=$?
        local parameter=$(cat $tempfile1)

#       echo "res=$?"
#       echo "par=$parameter"

#        [ $retval -eq 0 ]  || return 1
        [ -n "$parameter" ]  || return 1


        case $parameter in
        1)       _check_list  "\"my list\""  "item1" "item2"
                _item1=$(cat $tempfile3)
                echo " You used to _item1 = $_item1" > tttt;  _info_box "test" tttt; rm tttt;
                echo "_item1 = $_item1"

                ;;
        2)       _check_list  "\"my list\""  "item1" "item2"
                _item2=$(cat $tempfile3)
                echo " You used to _item2 = $_item2" > tttt;  _info_box "test" tttt; rm tttt;
                echo "_item2 = $_item2"
                ;;
        3)      _input_box "test box"
                 _info_box "test" $tempfile5 
                ;;


        4)      break 
                ;;
        esac

done

#_gui_main_end_
}


_gui_tk()
{
#_gui_tk_begin_


# title
title="GUI example"


[[ -f plot_gnuplot.me ]] && rm plot_gnuplot.me;
[[ -f plot.txt ]] && rm plot.txt;
[[ -f _gui.tk ]] && rm _gui.tk


# gnuplot script

cat <<_END > plot_gnuplot.me

/usr/bin/gnuplot <<_PLOT 2> /dev/null 

set terminal dumb
set noxtics
set notitle
set nokey
set noytics

plot \$what_to_plot

pause -1

_PLOT

_END



# example of TK/TCL use

cat <<_END > _gui.tk


wm title . "$title"
#grid [ttk::frame .c -padding "3 3 12 12"] -column 0 -row 0 -sticky nw
grid [::frame .c ] -column 0 -row 0 -sticky nw
grid columnconfigure . 0 -weight 1; grid rowconfigure . 0 -weight 1

# label
grid [::label .c.islbl -text " enter function to plot"] -column 0 -row 0 -sticky we
# text field
grid [::entry .c.text -width 27 -textvariable text] -column 1 -row 0 -sticky we

# buttons
grid [::button .c.save -text "Save script" -command save] -column 0 -row 1 -sticky we
grid [::button .c.exec -text "Plot script" -command execme] -column 1 -row 1 -sticky we
grid [::button .c.exit -text "Exit" -command exit] -column 2 -row 1 -sticky we

#plot area
grid [::label .c.plot  -textvariable plot] -column 0 -row 5 -sticky we

# menu bar

option add *tearOff 0
menu .menubar
. configure -menu .menubar

set m .menubar
menu \$m.file
menu \$m.help

\$m add cascade -menu \$m.file -label File
\$m add cascade -menu \$m.help -label Help

\$m.file add command -label "Close" -command "exit"
\$m.help add command -label "About" -command "print_about"

#menu .menumouse
#foreach i [Help Exit ] {.menumouse add command -label \$i}
#if {[tk windowingsystem]=="aqua"} {
#        bind . <2> "tk_popup .menumouse %X %Y"
#        bind . <Control-1> "tk_popup .menumouse %X %Y"
#} else {
#        bind . <3> "tk_popup .menumouse %X %Y"
#}

#.menumouse 

menu .menu
foreach i [list Exit About] {.menu add command -label \$i}
if {[tk windowingsystem]=="aqua"} {
        bind . <2> "tk_popup .menu %X %Y"
        bind . <Control-1> "tk_popup .menu %X %Y"
} else {
        bind . <3> "tk_popup .menu %X %Y"
}

.menu entryconfigure Exit -command exit; # change an entry
.menu entryconfigure About -command print_about; # change an entry


proc print_about {} {
tk_messageBox -message {
This is a simple TK application written by I. Marfin@2013 
	<DESY, Higgs group>
There are accelerators to be used:
ENTER -- transfer an expression to gnuplot
ALT+P -- plot
ESC -- Exit }
 

}


foreach w [winfo children .c] {grid configure \$w -padx 5 -pady 5}
focus .c.text
bind . <Return> {save}
bind . <Escape> {exit}
bind . <Alt-Key-p> {execme}
bind . <Key-F1> { print_about }


proc mysource {scriptname} {
    global env

    set envNew [exec /bin/bash -c "source \$scriptname; /usr/bin/env"]

    foreach line [lrange [split \$envNew \n] 0 end-1] {

        set eqpos [string first = \$line]
        set var   [string range \$line 0 [expr {\$eqpos-1}]]
        set value [string range \$line [expr {\$eqpos+1}] end]

        if { \$var != "PATH" } {
            set env(\$var) \$value
        } else {
            set env(\$var) [split \$value :]
        }
    }
}

proc execme {} {

global env

mysource ./plot.txt
puts \$::env(what_to_plot)
#exec chmod a+x ./plot_gnuplot.me
#exec /bin/sh -c "source plot_gnuplot.me"
#puts [exec ./plot_gnuplot.me]
puts [exec /bin/bash -c "source plot_gnuplot.me"]
set ::plot [exec /bin/bash -c "source plot_gnuplot.me"]
#set ::plot [exec /bin/sh -c "cat plot_gnuplot.me|sh"]

}

proc save {} {

set fo [open "./plot.txt" "w"] 
#set mytext "export what_to_plot=\"[.c.text get 1.0 end]\""
set mytext "export what_to_plot=\"\$::text\""
puts \$fo \$mytext
close \$fo
}






set width 1050
set height 550

set xcenter [expr { ( [winfo vrootwidth  .] - \$width  ) / 2 }]
set ycenter [expr { ( [winfo vrootheight .] - \$height ) / 2 }]


wm geometry . \${width}x\${height}+\${xcenter}+\${ycenter}


_END

cat _gui.tk | wish


#_gui_tk_end_
}


setup_docutils()
{
#setup_docutils_begin_
local help='
	
	set up the environment of docutils
'
export PYTHONPATH=/usr1/scratch/marfin/presentations/docutils:$PYTHONPATH
export PATH=/usr1/scratch/marfin/presentations/docutils/release/bin:$PATH

export PYTHONPATH=/usr1/scratch/marfin/presentations/Pygments-1.5/:$PYTHONPATH
export PATH=/usr1/scratch/marfin/presentations/Pygments-1.5/release/bin:$PATH

export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2pdf-0.92:$PYTHONPATH
export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2pdf-0.92/release/lib/python2.4/site-packages:$PYTHONPATH
export PATH=/usr1/scratch/marfin/presentations/rst2pdf-0.92/release/bin:$PATH
export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2beamer-0.6.3/release/lib/python2.4/site-packages/:$PYTHONPATH
export PATH=/usr1/scratch/marfin/presentations/rst2beamer-0.6.3/release/bin/:$PATH
export PYTHONPATH=/usr1/scratch/marfin/presentations/pelican-2.1/release/lib/python2.4/site-packages:$PYTHONPATH

echo "

How to install pygments from

http://www.deffbeff.com/blog/2009/06/using-pygments-with-docutils/

 cp ../Pygments-1.5/external/rst-directive.py  docutils/parsers/rst/directives/rst_directive.py
 cp ../Pygments-1.5/external/rst-directive.py  release/lib/python2.4/site-packages/docutils/parsers/rst/directives/rst_directive.py
 pico -w docutils/parsers/rst/directives/rst_directive.py
 pico -w release/lib/python2.4/site-packages/docutils/parsers/rst/directives/rst_directive.py
 pico -w release/bin/rst2html.py 
 pico -w release/bin/rst2latex.py 
 pico -w release/bin/rst2s5.py 

How to install setup tools:

 wget \"http://peak.telecommunity.com/dist/ez_setup.py\"
export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2pdf-0.92:$PYTHONPATH
export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2pdf-0.92/release/lib/python2.4/site-packages:$PYTHONPATH
python ez_setup.py --prefix /usr1/scratch/marfin/presentations/rst2pdf-0.92/release

How to install rst2pdf. This installs all: pygments, docutils!!!

 python setup.py install --prefix=/usr1/scratch/marfin/presentations/rst2pdf-0.92/release

How to install rst2beamer:

export PYTHONPATH=/usr1/scratch/marfin/presentations/rst2beamer-0.6.3/release/lib/python2.4/site-packages/:$PYTHONPATH
python setup.py install --prefix=/usr1/scratch/marfin/presentations/rst2beamer-0.6.3/release  


WIKI on Restructed Text and more useful tools:

http://stackoverflow.com/questions/2746692/restructuredtext-tool-support

How to install Pelican

download it (2.1 version is good for python2.4)

https://github.com/getpelican/pelican/

install it

mkdir -p  /usr1/scratch/marfin/presentations/pelican-2.1/release/lib/python2.4/site-packages
export PYTHONPATH=/usr1/scratch/marfin/presentations/pelican-2.1/release/lib/python2.4/site-packages:$PYTHONPATH
mkdir release; python setup.py install --prefix=\$(PWD)/release/

"

#setup_docutils_end_

}


email_attachment() {
#email_attachment_begin_
local help='

send emails with attachment

#$1 -- to
#$2 -- cc
#$3 -- subject
#$4 -- body
#$5 -- file to be attached

Example:

email_attachment iggy.floyd.de@googlemail.com " " "Hi" "Hi"  21.03.12_higgsDesy.tgz
'

local title=`echo $@ | tr ' ' '_'`;


if [ $# -lt 4 ]; then print_help "$0" "$help"; return 1; fi;


    to="$1"
    cc="$2"
    subject="$3"
    body="$4"
    filename="${5:-''}"
    boundary="_====_blah_====_$(date +%Y%m%d%H%M%S)_====_"
    {

       print -- "To: $to"
        print -- "Cc: $cc"
        print -- "Subject: $subject"
        print -- "Content-Type: multipart/mixed; boundary=\"$boundary\""
        print -- "Mime-Version: 1.0"
        print -- ""
        print -- "This is a multi-part message in MIME format."
        print -- ""
        print -- "--$boundary"
        print -- "Content-Type: text/plain; charset=ISO-8859-1"
        print -- ""
        print -- "$body"
        print -- ""
        if [[ -n "$filename" && -f "$filename" && -r "$filename" ]]; then
            print -- "--$boundary"
            print -- "Content-Transfer-Encoding: base64"
            print -- "Content-Type: application/octet-stream; name=$filename"
            print -- "Content-Disposition: attachment; filename=$filename"
            print -- ""
            print -- "$(perl -MMIME::Base64 -e 'open F, shift; @lines=<F>; close F; print MIME::Base64::encode(join(q{}, @lines))' $filename)"
            print -- ""
        fi
        print -- "--${boundary}--"
    } | /usr/lib/sendmail -oi -t
#email_attachment_end_
}


create_config_rst_blog()
{
#create_config_rst_blog_begin_

local help='

run program create_config_rst_blog to create several config files:   

local_settings.py -- the full settings list
settings.py -- template of settings list 
short_local_settings.py -- the short settings list (predefined to some local place of the blog)
.gitignore -- contains a list of files to ingore by github


The following list of fields are supported

\$1 AUTHOR 
\$2 SITENAME
\$3 SITEURL
\$4 EMAIL

Example:

	create_config_rst_blog  "Igor Marfin" "MyURL" "http://igormarfin.github.com/pages" "iggy.floyd.de@gmal.comNOTSPAM" 

'

if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

author="$1"
[ -z "$author" ] && author="Igor Marfin"

sitename="$2"
[ -z "$sitename" ] && sitename="Igor Marfin's pages"


siteurl="$3" 
[ -z "$siteurl" ] && siteurl="/mnt/WorkingPlace/myweb/output"

email="$4"
[ -z "$email" ] && email="iggy.floyd.de@gmal.comNOTSPAM" 


# create  local_settings.py
echo "

#!/usr/bin/env python 


AUTHOR = '$author'
SITENAME = '$sitename'
SITEURL = '$siteurl'
TIMEZONE = 'Europe/Berlin'


GITHUB_URL = 'http://github.com/igormarfin/pages'
DISQUS_SITENAME = 'XXXXXXXXXX'
EMAIL = 'iggy,floyd.de@gmail.com'
PDF_GENERATOR = False
REVERSE_CATEGORY_ORDER = True
LOCALE = 'en_US'
DEFAULT_PAGINATION = 5

THEME = 'themes/tuxlite_tbs'

FEED_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

LINKS = (('DESY', 'http://www.desy.de'),
		('CERN', 'http://www.cern.ch'),
     )

SOCIAL = (('google', 'http://google.com'),
          ('github', GITHUB_URL),)

OUTPUT_PATH = 'output'
PATH = 'src'

#ARTICLE_URL = 'posts/{date:%Y}/{date:%m}/{slug}/'
#ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{slug}/index.html'
ARTICLE_URL = 'posts/{date:%Y}/{date:%m}/{date:%H%M}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%H%M}/{slug}/index.html'

GOSQUARED_SITENAME = 'XXX-YYYYYY-X'

# global metadata to all the contents
#DEFAULT_METADATA = (('yeah', 'it is'),)

# static paths will be copied under the same name
STATIC_PATHS = ['images', ]

# A list of files to copy from the source to the destination
#FILES_TO_COPY = (('extra/robots.txt', 'robots.txt'),)


" >  local_settings.py


# create  settings.py
echo "

#!/usr/bin/env python 


AUTHOR = 'xxxxxxxx xxxxxxxxxxxxx'
SITENAME = "xxxxxxxxxxxx"
SITEURL = 'http://xxxxxxxxxxxxxxxxx'
TIMEZONE = "Europe/Madrid"

GITHUB_URL = 'http://github.com/XXXXX'
DISQUS_SITENAME = 'XXXXXXXXXX'
EMAIL = 'XXXXXXXXXXXXXX@gmail.com'
PDF_GENERATOR = False
REVERSE_CATEGORY_ORDER = True
LOCALE = 'en_US'
DEFAULT_PAGINATION = 5

THEME = 'iris'

FEED_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

LINKS = (('XXXXX XXXX', 'http://YYYYYYYYYYY.ZZZ'),)

SOCIAL = (('twitter', 'http://twitter.com/XXXXXX'),
          ('linkedin', 'http://www.linkedin.com/in/XXXXXXX'),
          ('github', GITHUB_URL),)

OUTPUT_PATH = 'output'
PATH = 'src'

ARTICLE_URL = "posts/{date:%Y}/{date:%m}/{slug}/"
ARTICLE_SAVE_AS = "posts/{date:%Y}/{date:%m}/{slug}/index.html"

GOSQUARED_SITENAME = 'XXX-YYYYYY-X'

# global metadata to all the contents
#DEFAULT_METADATA = (('yeah', 'it is'),)

# static paths will be copied under the same name
STATIC_PATHS = ['images', ]

# A list of files to copy from the source to the destination
#FILES_TO_COPY = (('extra/robots.txt', 'robots.txt'),)


" > settings.py


# create  short_local_settings.py
echo "

#!/usr/bin/env python 



DEFAULT_CATEGORY = 'Uncategorized'

TWITTER_USERNAME = ''

PDF_GENERATOR = False
REVERSE_CATEGORY_ORDER = True
DEFAULT_PAGINATION = 10

#FEED_RSS = 'feeds/all.rss.xml'
#CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

SOCIAL = ()
LINKS =  ()
#OUTPUT_PATH = 'myweb/output'
THEME='themes/tuxlite_tbs'
SITEURL = '/mnt/WorkingPlace/myweb/output'
TIMEZONE = 'Europe/Berlin'
DEFAULT_DATE_FORMAT = ('%a %d %B %Y')
#DEFAULT_TIME_FORMAT = ('%H:%M:%S')
AUTHOR = u'$author'
SITENAME = u'$sitename'



"> short_local_settings.py


# create  .gitignore
echo "

#Custom
local_settings.py
short_local_settings.py
output

#Python
*.py[cod]

# C extensions
*.so
*.C
*.cpp

# Packages
*.egg
*.egg-info
dist
build
eggs
parts
bin
var
sdist
develop-eggs
.installed.cfg
lib
lib64

# Installer logs
pip-log.txt


" > .gitignore

#create_config_rst_blog_end_
}

deploy_rst_blog()
{
#deploy_rst_blog_begin_

local help='

run program deploy_rst_blog to release your blog at the github.

The following list of fields are supported
\$1 settings_file :  short_local_settings.py or local_settings.py
\$2 commit_message
\$3 user name like igormarfin
\$4 blog name like pages


1) First, create at github your repository for the blog, lets say, "igormarfin/pages"
2) Then, do ini of your repository:

touch README.md
git init
git pull origin master
git add README.md
git commit -m "first commit"
git push git@github.com:igormarfin/pages master
#git remote add origin https://github.com/igormarfin/pages.git
#git push -u origin master

2.1)  if  you have created your blog git, instead of 1), do cloning

git clone https://github.com/igormarfin/pages.git

We assume that you are in a root folder of your "posts" during this time.
4) Finally, you deploy an update of your pages:

deploy_rst_blog  "local_settings.py" "My first post" "igormarfin" "pages"

'

if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

settings_file="$1"
[ -z "$settings_file" ] && settings_file="./local_settings.py"

commit_message="$2"
[ -z "$commit_message" ] && commit_message="My first post"
commit_message=`echo "\"$commit_message\""`

username="$3"
[ -z "$username" ] && username="igormarfin"

blogname="$3"
[ -z "$blogname" ] && blogname="pages"


#it doesn't work

#git add .
#git commit -m "$commit_message"
#git push "git@github.com:$username/$blogname" master
##git push origin master
#git branch gh-pages
#git merge master
#pelican posts/ -s "$settings_file"  -o ./
#git add .
#git commit -m "$commit_message"
##git push origin gh-pages
#git push "git@github.com:$username/$blogname"  gh-pages
#git checkout master

git checkout --orphan gh-pages
git pull origin gh-pages
pelican posts/ -s "$settings_file"  -o ./
git add .
#git add images 
#git add images/*
git commit -m "$commit_message"
git push origin gh-pages


#deploy_rst_blog_end_
}

add_rst_blog_v2()
{
#add_rst_blog_v2_begin_

local help='

run program add_rst_blog to add   a post (from the template) to the pelican-based blog
Then someone can  use pelican <dir_blog> to publish the post

The following list of fields are required

\$1 Author 
\$2 Category
\$3 Tags
\$4 Title
\$5 Subtitle
\$6 post name like "post.rst"
\$7 Image folder, if a post is published with figures

The add_rst_v2_() creates a simple database presented by the file posts.db. The file is needed for correct removing the post from GIT.
The designed format of the file:

# id ;  time ;  relative path to rst; post name; a name of image folder

Example of making a post

#    mkdir -p  ./posts/`date +%Y`/`date +%m`
#	add_rst_blog "Igor Marfin" "About me" "no-tags" "About me" "Example of the pelican use" > posts/`date +%Y`/`date +%m`/about_me.rst

    _post=./posts/`date +%Y`/`date +%m`/`date +%H%M`
    mkdir -p  $_post
	add_rst_blog "Igor Marfin" "About me" "no-tags" "About me" "Example of the pelican use" > $_post/about_me.rst
    pelican posts/ -s ./short_local_settings.py 
    cd ./output; python -m SimpleHTTPServer


Example how to post a text with figures (from the begining)

relpath=yes; prepare_rst2pdf_template 2 "500:700" static/plots53x2011HLT/ png 8 "HLT BTAG 2011 MENU RelVal53x" > 06.09.13_marfin_2011legacyMenu.In53x.rst
cd /mnt/WorkingPlace/myblog/pages/
cp -r /mnt/WorkingPlace/presentation/06.09.13_TSG_meeting/static/plots53x2011HLT  posts/
# fix header
nano -w 06.09.13_marfin_2011legacyMenu.In53x.rst 
# fix  add to STATIC_PATHS=["images","plots53x2011HLT"]
nano -w local_settings.py
nano -w short_local_settings.py
_post=./posts/`date +%Y`/`date +%m`/`date +%H%M`
mkdir -p  $_post
cp 06.09.13_marfin_2011legacyMenu.In53x.rst $_post
Changeword "image:: static" "image:: http://igormarfin.github.io/pages/static"  $_post/06.09.13_marfin_2011legacyMenu.In53x.rst $_post/06.09.13_marfin_2011legacyMenu.In53x.rst
deploy_rst_blog  "local_settings.py" "Control plots of 2011 HLT RelVal53x study" "igormarfin" "pages"


Example how to create and deploy your blog:

git clone https://github.com/igormarfin/pages.git
cd pages
cp -r /mnt/WorkingPlace/myweb/themes  .
create_config_rst_blog ;
add_rst_blog  ;
deploy_rst_blog ;
'

[ ! -f  "posts.db" ] && touch posts.db


if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

author="$1"
[ -z "$author" ] && author="I. Marfin"

category="$2"
[ -z "$category" ] && category="A test category"


tags="$3" 
[ -z "$tags" ] && tags="no-tags"

title="$4"
[ -z "$title" ] && title="A title"

subtitle="$5"
[ -z "$subtitle" ] && subtitle="A subtitle"


postname="$6"
id=`echo $RANDOM`
[ -z "$postname" ] && postname="post_$id".rst

imagefolder="$7"

# create a post

 _post=./posts/`date +%Y`/`date +%m`/`date +%H%M`
mkdir -p  $_post
add_rst_blog "$author" "$category" "$tags" "$title" "$subtitle" > ${_post}/${postname}



# fill db file
# id ;  time ;  relative path to rst; post name; a name of image folder

echo "$id ;   `date +"%Y-%m-%d %T"` ;  $_post ; $postname ; $imagefolder" >> posts.db

# fix settings
Changeword "STATIC_PATHS" "="  $_post/06.09.13_marfin_2011legacyMenu.In53x.rst $_post/06.09.13_marfin_2011legacyMenu.In53x.rst


#add_rst_blog_v2_end_
}



add_rst_blog()
{
#add_rst_blog_begin_

local help='

run program add_rst_blog to add   a post (from the template) to the pelican-based blog
Then someone can  use pelican <dir_blog> to publish the post

The following list of fields are supported

\$1 Author 
\$2 Category
\$3 Tags
\$4 Title
\$5 Subtitle

Example of making a post

#    mkdir -p  ./posts/`date +%Y`/`date +%m`
#	add_rst_blog "Igor Marfin" "About me" "no-tags" "About me" "Example of the pelican use" > posts/`date +%Y`/`date +%m`/about_me.rst

    _post=./posts/`date +%Y`/`date +%m`/`date +%H%M`
    mkdir -p  $_post
	add_rst_blog "Igor Marfin" "About me" "no-tags" "About me" "Example of the pelican use" > $_post/about_me.rst
    pelican posts/ -s ./short_local_settings.py 
    cd ./output; python -m SimpleHTTPServer


Example how to post a text with figures (from the begining)

relpath=yes; prepare_rst2pdf_template 2 "500:700" static/plots53x2011HLT/ png 8 "HLT BTAG 2011 MENU RelVal53x" > 06.09.13_marfin_2011legacyMenu.In53x.rst
cd /mnt/WorkingPlace/myblog/pages/
cp -r /mnt/WorkingPlace/presentation/06.09.13_TSG_meeting/static/plots53x2011HLT  posts/
# fix header
nano -w 06.09.13_marfin_2011legacyMenu.In53x.rst 
# fix  add to STATIC_PATHS=["images","plots53x2011HLT"]
nano -w local_settings.py
nano -w short_local_settings.py
_post=./posts/`date +%Y`/`date +%m`/`date +%H%M`
mkdir -p  $_post
cp 06.09.13_marfin_2011legacyMenu.In53x.rst $_post
Changeword "image:: static" "image:: http://igormarfin.github.io/pages/static"  $_post/06.09.13_marfin_2011legacyMenu.In53x.rst $_post/06.09.13_marfin_2011legacyMenu.In53x.rst
deploy_rst_blog  "local_settings.py" "Control plots of 2011 HLT RelVal53x study" "igormarfin" "pages"


Example how to create and deploy your blog:

git clone https://github.com/igormarfin/pages.git
cd pages
cp -r /mnt/WorkingPlace/myweb/themes  .
create_config_rst_blog ;
add_rst_blog  ;
deploy_rst_blog ;

'


if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

author="$1"
[ -z "$author" ] && author="I. Marfin"

category="$2"
[ -z "$category" ] && category="A test category"


tags="$3" 
[ -z "$tags" ] && tags="no-tags"

title="$4"
[ -z "$title" ] && title="A title"

subtitle="$5"
[ -z "$subtitle" ] && subtitle="A subtitle"


local len=`echo $title | wc -c`
local title2="#";
for ((i=1;i<$len;i++)); do title2=`echo ${title2}'#'`;done

local len2=`echo $subtitle | wc -c`
local title3="-";
for ((i=1;i<$len2;i++)); do title3=`echo ${title3}'-'`;done


echo " "
echo $title
echo $title2
echo; echo ;echo;

echo ":date:"  `date +"%Y-%m-%d %T"`
echo ":category: $category "
echo ":tags:  $tags"
echo ":author:	 $author "

echo ; echo; echo;

echo "

$subtitle
$title3


"

echo "

Hi!

This is my first rst post!

And this is a link to a page_


.. _page: http://desy.de/



"



#add_rst_blog_end_
}

create_style_rst2pdf()
{
#create_style_rst2pdf_begin_

local help='
 to create a style file for rst2pdf

'

if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;


echo " 

{
  "pageSetup" : {
    "firstTemplate": "coverPage"
  },
  "pageTemplates" : {
    "coverPage": {
        "frames": [
            ["0cm", "0cm", "100%", "100%"]
          ],
#        "background" : "biohazard.pdf",
        "showHeader" : false,
        "showFooter" : false
    }
  },
  "styles" : [
    ["title", {
        "fontName": "White Rabbit",
        "fontSize": 96
    }],
        
    ["thin" , {
    "parent" : "bodytext",
    "width" : "65%",
    "alignment": "TA_JUSTIFY",
    "hyphenation": true,
    "language": "es_ES"
    }
    ],
    ["right", {
        "parent": "bodytext",
        "alignment": "right"
    }],
    ["centered", {
        "parent": "bodytext",
        "alignment": "center"
    }],
    ["headertable" , {
    "parent" : "table",
    "colWidths": ["20%","60%","20%"],
    "commands": []
    }
    ]
   ]
}

"



#create_style_rst2pdf_end_
}

prepare_rst_doc()
{
#prepare_rst_doc_begin_

local help='

run program prepare_rst_doc to create  a template of the rst document.
Then someone can  use any program like rst2pdf, rst2latex, rst2html, rst2wiki to get a final version
of your document/manual/wiki/how-to etc

The following list of fields are supported

\$1 Author 
\$2 Contact
\$3  Status
\$4 Version
\$5 title

Example:

	prepare_rst_doc "I. Marfin" "<marfin@mail.desy.de>" "work in progress" "v0.1" "Example of HOW-TO" > HOW-TO.rst
    create_style_rst2pdf > rst2pdf.style
    rst2pdf HOW-TO.rst -s rst2pdf.style

'


if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

author="$1"
[ -z "$author" ] && author="I. Marfin"

contact="$2"
[ -z "$contact" ] && contact="<marfin@mail.desy.de>"


status="$3" 
[ -z "$status" ] && status="Work in progress"

version="$4"
[ -z "$version" ] && version="v0.1"

title="$5"
[ -z "$title" ] && title="Example of HOW-TO RST"

local len=`echo $title | wc -c`
local title2="=";
for ((i=1;i<$len;i++)); do title2=`echo ${title2}=`;done


echo " "
echo $title2
echo $title
echo $title2
echo; echo ;echo;

echo ":Author:	 $author "
echo ":Contact: $contact "
echo ":Status:  $status"
echo ":Organization: DESY"
echo ":Version: $version"
echo ":Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.
"


echo ":date:"  `date +%D`

echo "
:field name:
    Generic bibliographic fields may contain multiple body elements.

    Like this.
"


echo "

:Dedication:

    For Docutils users & co-developers.
"



#echo ".. include:: /home/school/rstDefs_v2.txt"


echo "

:abstract:

         This document is a demonstration of the reStructuredText markup
         language, containing examples of all basic reStructuredText
         constructs and many advanced constructs. The topic is relevant for DESY Higgs group


"

echo "

..  meta::
   :keywords: reStructuredText, demonstration, demo, parser
   :description lang=en: A demonstration of the reStructuredText
       markup language, containing examples of all basic
       constructs and many advanced constructs.
"

echo "

.. header::

   .. oddeven::

      .. class:: headertable

      +---+---------------------+----------------+
      |   |.. class:: centered  |.. class:: right|
      |   |                     |                |
      |   |Section ###Section###|Page ###Page### |
      +---+---------------------+----------------+


      .. class:: headertable

      +---------------+---------------------+---+
      |               |.. class:: centered  |   |
      |               |                     |   |
      |Page ###Page###|Section ###Section###|   |
      +---------------+---------------------+---+



.. contents:: Table of Contents
.. section-numbering::


.. raw:: pdf

   PageBreak oneColumn

"

echo "

.. Note:: That's only EXAMPLE. Please, don't consider it seriously!


"


# section 1
echo "

Section First (stored in context)
=================================

Section Title(1)
-----------------
Just an example of the code blocks:


.. code:: bash

    gedit pohl.m
    ./test.sh
    printf \"%s\" \"OK\"


.. code-block:: c

   #include <iostream>
    void test() { 
    std::cout<<\"test\"<<std::endl; 
    return ;

    }

.. code:: c

   #include <iostream>
    void test() { 
    std::cout<<\"test\"<<std::endl; 
    return ;

    }


.. code-block:: python


   def test():
   \"\"\" test \"\"\"
    print \"How are you\"?

    return


"

# section 2

echo "

Section Second 
==================================

Bullet Lists
------------

- A bullet list

  + Nested bullet list.
  + Nested item 2.

- Item 2.

  Paragraph 2 of item 2.

  * Nested bullet list.
  * Nested item 2.

    - Third level.
    - Item 2.

  * Nested item 3.

Enumerated Lists
----------------

1. Arabic numerals.

   a) lower alpha)

      (i) (lower roman)

          A. upper alpha.

             I) upper roman)

2. Lists that don't start at 1:

   3. Three

   4. Four

   C. C

   D. D

   iii. iii

   iv. iv

#. List items may also be auto-enumerated.

Definition Lists
----------------

Term
    Definition
Term : classifier
    Definition paragraph 1.

    Definition paragraph 2.
Term
    Definition


"




# section 3
echo "

Section Third
==============



Literal Blocks
--------------

Literal blocks are indicated with a double-colon (\"::\") at the end of
the preceding paragraph (over there \`\`-->\`\`).  They can be indented::

    if literal_block:
        text = 'is left as-is'
        spaces_and_linebreaks = 'are preserved'
        markup_processing = None


Or they can be quoted without indentation::

>> Great idea!
>
> Why didn't I think of that?


Block Quotes
------------

Block quotes consist of indented body elements:

    My theory by A. Elk.  Brackets Miss, brackets.  This theory goes
    as follows and begins now.  All brontosauruses are thin at one
    end, much much thicker in the middle and then thin again at the
    far end.  That is my theory, it is mine, and belongs to me and I
    own it, and what it is too.

    -- Anne Elk (Miss)


Tables
------

Here's a grid table followed by a simple table:

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: \`\`-->\`\`    |          |
+------------------------+-----------------------+----------+

=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
False  True   True
True   True   True
=====  =====  ======





"


#section 4

echo "

Section Fourth
===============


Citations
---------

.. [CIT2002] Citations are text-labeled footnotes. They may be
   rendered separately and differently from footnotes.

Here's a reference to the above, [CIT2002]_ citation.

Footnotes
---------

.. [1] A footnote contains body elements, consistently indented by at
   least 3 spaces.

   This is the footnote's second paragraph.

.. [#label] Footnotes may be numbered, either manually (as in [1]_) or
   automatically using a \"#\"-prefixed label.  This footnote has a
   label so it can be referred to from multiple places, both as a
   footnote reference ([#label]_) and as a hyperlink reference
   (label_).

.. [#] This footnote is numbered automatically and anonymously using a
   label of \"#\" only.

.. [*] Footnotes may also use symbols, specified with a \"*\" label.
   Here's a reference to the next footnote: [*]_.

.. [*] This footnote shows the next symbol in the sequence.


"

# section 5
echo  "

Section Fifth
===============



Targets
-------

.. _example:

This paragraph is pointed to by the explicit \"example\" target. A
reference can be found under \`Section Second\`_, above. 

Section headers are implicit targets, referred to by name. See
Targets_.

Explicit external targets are interpolated into references such as
\"Python_\" or such as [#python2]_.


.. _Python: http://www.python.org/

.. [#python2] http://www.python.org/  


Targets may be indirect and anonymous.  Thus \`this phrase\`__ may also
refer to the Targets_ section.

__ Targets_

"

#section 6
echo "

Section Sixth
==============

More details
-------------

you can find more details here [#demoRst]_ and here [#demoHtml]_



.. [#demoRst] http://docutils.sourceforge.net/docs/user/rst/demo.txt http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.txt http://docutils.sourceforge.net/rst.txt



..  [#demoHtml] http://docutils.sourceforge.net/docs/user/rst/demo.html#inline-markup
"


#section 7
echo "

Section Seventh
================

HOW to obtain from RST different  outputs
-----------------------------------------


* Obtain HTML

.. code :: bash

    rst2html.py HOW-TO.rst --syntax-highlight=\"long\" > HOW-TO.html

* Obtain  PDF

.. code :: bash

   rst2pdf   HOW-TO.rst 

* Obtain TeX

.. code :: bash

   rst2latex   HOW-TO.rst  > HOW-TO.tex


* Obtain twiki (for CERN twiki)

.. code :: bash

   cat   HOW-TO.rst | rst2twiki.py  > HOW-TO.twiki


* Obtain wiki 

.. code :: bash

   cat   HOW-TO.rst | rst3wiki.py  > HOW-TO.wiki





Good Luck!

"


#prepare_rst_doc_end_
}

prepare_rst2beamer_template()
{
#prepare_rst2beamer_template_begin_

local help='

run program rst2pdf to create presentation

\$1 how many plots per slides
\$2 default size of plot
\$3 where to look for plots
\$4 what extenstion use for plots: jpg, png,eps,pdf etc
\$5 title

Example:
  prepare_rst2beamer_template 4 "300:400" "" eps  > test6.rst
  run_rst2beamer  test6.rst "latex dvipdf"   --theme=progressbar
  evince test6.rst.pdf


'
if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;




local _many=1;
local _size=700;
local _sizeW=$_size;

if [ -n "$1" ]; then _many=$1; fi

if [ $_many -eq 2 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=600
                _sizeW=800
	fi
	if [ $_many -eq 3 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=400
                _sizeW=500
	fi

	if [ $_many -eq 4 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=380
                _sizeW=600
	fi

if [ -n "$2" ] 
then 
	_size=`echo $2 | awk -F':'  '{print $1}'`
    _sizeW=`echo $2 | awk -F':' '{print $2}'`

fi 


echo "

.. role:: raw-tex(raw)
    :format: latex html


.. default-role:: raw-tex


"

where=""
if [ -n "$3" ]; then where="$3"; fi

if [ -n "$4" ]; then  ext="$4"; else ext="jpg"; fi
if [ -n "$5" ]; then  title="$5"; else title="Some plots"; fi




find `pwd` -iname "*.$ext" | grep -v "legend" | grep "$where"  | uniq | xargs -I {} echo " echo -n \".. |{}| image:: \";  echo {}; echo \"            :width: $_sizeW
            :height: $_size

\"  " |sh


a=($(find `pwd` -iname "*.$ext" | grep -v "legend"   | grep "$where"  | sort -r  ));



len=`echo $title | wc -c`
local title2="=";
for ((i=1;i<$len;i++)); do title2=`echo ${title2}=`;done



echo " "
echo $title2
echo $title
echo $title2
echo "|
|
|
"
echo ":author: \"I. Marfin <igor.marfin@desy.de> \" "
echo " "
echo ".. include:: /home/school/rstDefs_v2.txt"
echo " "

echo "

Figures
--------

dummy
~~~~~~

"

local startpos=0;

[[ -n "${a[0]}"  ]] &&  startpos=1;

#echo "startpos=$startpos"


if [ $_many -eq 1 ]
then

c=`echo "scale=0; ${#a[@]}"|bc`

#echo $c
#echo $c2
#i=1
#echo ${a[$((i-$startpos))]}
#i=3
#echo ${a[$((i-$startpos))]}




for ((i=1;i<=$((c));i+=1 ))
do

#echo ${a[$((i-$startpos))]}

# determine the len of caption
echo "Figure:  "   `basename ${a[$((i-$startpos))]} |  sed  -ne 's/_//g'  -ne 's/_can_//g'  -ne  "s/.$ext//g"   -ne 's/_0//g' -e 'p' ` >| tmp.capt;  
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


echo -n "Figure: "; 
echo   `basename ${a[$((i-$startpos))]} |  sed  -ne 's/_//g' -ne 's/_can_//g'  -ne  "s/.$ext//g"   -ne 's/_0//g' -e 'p' `; 
#echo "----------------------"
echo  $capt
echo " "
echo  "|${a[$((i-$startpos))]}| "
echo 
echo

done





fi



if [ $_many -eq 2 ]
then

c=`echo "scale=0; ${#a[@]}/2"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*2"|bc`

#echo $c
#echo $c2
#i=1
#echo ${a[$((i-$startpos))]}
#i=3
#echo ${a[$((i-$startpos))]}




for ((i=1;i<=$((c*2));i+=2 ))
do

#echo ${a[$((i-$startpos))]}

# determine the len of caption
(echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} |  sed  -ne 's/_//g'  -ne 's/_can_//g'  -ne  "s/.$ext//g"   -ne 's/_0//g' -e 'p' `; [[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+1-$startpos))]} | sed   -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e "p" ` ) )> tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} |  sed  -ne 's/_//g' -ne 's/_can_//g'  -ne  "s/.$ext//g"   -ne 's/_0//g' -e 'p' `; [[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e "p" ` )
#echo "----------------------"
echo  $capt
echo " "
echo -n "|${a[$((i-$startpos))]}| "
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo "|${a[$((i+1-$startpos))]}|"
echo 
echo
done




if [ $c2 -gt 0 ] 
then 

# determine the len of caption
(echo -n "Figure: "; echo -n  `basename ${a[$((c*2+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'` )> tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


echo -n "Figure: "; echo -n  `basename ${a[$((c*2+1-$startpos))]} | sed   -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'` 
echo " "
echo $capt
#echo "----------------------"
echo " "

 echo  "|${a[$((c*2+1-$startpos))]}|"

echo
echo
fi

fi

if [ $_many -eq 3 ]
then

c=`echo "scale=0; ${#a[@]}/3"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*3"|bc`

#echo "c=$c"
#echo "c2=$c2"

for ((i=1;i<=$((c*3));i+=3 ))
do

# determine the len of caption
(echo -n "Figure: "; echo -n  `basename ${a[$((i - $startpos)) ]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2 -$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+2-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
) > tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done



echo -n "Figure: "; echo -n  `basename ${a[$((i - $startpos)) ]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2 -$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+2-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
#echo "----------------------"
echo "$capt"
echo " "

echo -n "|${a[$((i-$startpos))]}| " 
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo -n "|${a[$((i+1-$startpos))]}| "
[[ -n "${a[$((i+2-$startpos))]}" ]] && echo -n "|${a[$((i+2-$startpos))]}|"

echo 
echo
echo
	
done



if [ $c2 -gt 0 ]
then

# determine the len of caption
 (echo -n "Figure: ";  echo -n  `basename ${a[$((c*3+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p' `) > tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


 (echo -n "Figure: ";  echo -n  `basename ${a[$((c*3+1-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p' `); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*3+2-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
echo " "
#echo "----------------------"
echo "$capt"

echo " "

[[ $c2 -gt 0 ]] && echo -n "|${a[$((c*3+1-$startpos))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2-$startpos))]}" ]] && echo -n " |${a[$((c*3+2-$startpos))]}|"

echo
echo

fi
fi


if [ $_many -eq 4 ]
then
c=`echo "scale=0; ${#a[@]}/4"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*4"|bc`

for ((i=1;i<=$((c*4));i+=4 ))
do

# determine the len of caption
(echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed  -ne 's/_//g'  -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+2-$startpos))]} | sed   -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
[[ -n "${a[$((i+3-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+3-$startpos))]} | sed   -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
) > tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+2-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
[[ -n "${a[$((i+3-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+3-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
#echo "----------------------"
echo "$capt"

echo " "
echo -n "|${a[$((i-$startpos))]}|" 
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo " |${a[$((i+1-$startpos))]}|"

echo " "


echo -n "|${a[$((i+2-$startpos))]}| "
[[ -n "${a[$((i+3-$startpos))]}" ]] && echo  " |${a[$((i+2-$startpos))]}|"

echo 
echo
done

if  [ $c2 -gt 0 ]
then 

# determine the len of caption
(echo -n "Figure: " ;  echo -n  `basename ${a[$((c*4+1-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne  "s/.$ext//g" -e 'p'`); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*4+2-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3-$startpos))]}" ]] && (echo -n "  ";   echo -n  `basename ${a[$((c*4+3-$startpos))]} | sed  -ne 's/_//g' -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`) > tmp.capt
len=`cat tmp.capt| wc -c`;
rm tmp.capt;
local capt="=";
for ((j=1;j<$len;j++)); do capt=`echo ${capt}=`;done


(echo -n "Figure: " ;  echo -n  `basename ${a[$((c*4+1-$startpos))]} | sed   -ne 's/_//g' -ne 's/_can_//g' -ne "s/_0//g" -ne  "s/.$ext//g" -e 'p'`); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*4+2-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g"  -ne 's/_//g' -e 'p'`);
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3-$startpos))]}" ]] && (echo -n "  ";   echo -n  `basename ${a[$((c*4+3-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g"  -ne 's/_//g' -e 'p'`)
echo " "
#echo "----------------------"
echo "$capt"
echo " "



[[ $c2 -gt 0 ]] && echo -n  "|${a[$((c*4+1-$startpos))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2-$startpos))]}" ]] && echo " |${a[$((c*4+2-$startpos))]}|"

echo " "

echo " "

echo "|${a[$((c*4+3-$startpos))]}|"

echo
echo
fi
fi


	




#prepare_rst2beamer_template_end_
}

run_rst2beamer()
{
#run_rst2beamer_begin_
local help='

run program rst2beamer to create presentation

#$1 -- file to be processed 
#$2 -- do or not latex and dvipdf
#$2, $3 etc -- options of rst2pdf. --help describes all of them


if you want to use tikz
http://www.texample.net/tikz/examples/connecting-text-and-graphics/

Example of run:

please modify the title for corresponded theme!

run_rst2beamer  test6.rst "latex dvipdf"   --theme=progressbar
run_rst2beamer  test4.rst "latex dvipdf"   --theme=lankton-keynote
run_rst2beamer  test4.rst "latex dvipdf"   --theme=CEA
run_rst2beamer  test4.rst "latex dvipdf"   --theme=HongKong
run_rst2beamer  test4.rst "latex dvipdf"   --theme=Stockton
run_rst2beamer  test4.rst "latex dvipdf"   --theme=BIG
run_rst2beamer  test4.rst "latex dvipdf"    --stylesheet=FlipBeamer/latexDARK.tex
run_rst2beamer  test4.rst "latex dvipdf"    --stylesheet=FlipBeamer/latexLIGHT.tex
run_rst2beamer  test4.rst "latex dvipdf"    --theme=Flip
run_rst2beamer  DPG2013_marfin.rst "pdflat"   --theme=progressbar
run_rst2beamer  DPG2013_marfin.rst "pdflat"   --theme=Flip --overlaybullets=false
run_rst2beamer  DPG2013_marfin.rst "pdflat"   --stylesheet=/home/school/../FlipBeamer/latexDARK.tex --overlaybullets=false
run_rst2beamer 25.03.13_marfin_Background_templates_merging.rst "pdflat" --theme=Boadilla --overlaybullets=false

source title_page_setting.lst; run_rst2beamer  DPG2013_marfin_v0.4.rst  "pdflat"   --stylesheet=/home/school/../FlipBeamer/latexLIGHT.tex --overlaybullets=false

source title_page_setting.lst; run_rst2beamer 27.06.13_marfin_MSSM_Higgs_search.rst "pdflat" --stylesheet=/home/school/../FlipBeamer/latexLIGHT.tex --overlaybullets=false

There are a list of  shell env. variables which  can be used to customize the title page:

rst_author
rst_institute
rst_additional_text
rst_logo (yes/no) -- DOESNT WORK "with latex dvipdf", use "unset rst_logo" to avoid problems!
rst_date
rst_short_title
rst_short_institute
rst_toc (table if we want table)
rst_nice_footer (yes/no) -- add slide number to presentation
rst_short_author
rst_short_logo (yes/no) -- if you want logo in frame titles
rst_background (yes/no) -- if you want transparent background wallpaper

'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;
if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;


local file_name="$1"
local rst2beamer="/usr/local/bin/rst2beamer";
local output=" ${file_name}.tex";
local dolatex=`echo $2 | grep "latex"`
local dopdflatex=`echo $2 | grep "pdflat"`
local dodvipdf=`echo $2 | grep "dvipdf"`



shift;
shift;
$rst2beamer   $*    $file_name  $output

##fix problems  with tex
#http://tex.stackexchange.com/questions/32812/beamer-footer-with-current-slide-number-but-not-total-slide-number

if [ -n "$dolatex" -o -n "$dopdflatex" ]
then
title=`findLine pdftitle =  $output | sed -ne 's/pdftitle=/\\\title/g' -ne 's/,//g' -e 'p'`
title3=`findLine pdftitle =  $output | sed -ne 's/pdftitle=//g' -ne 's/,//g' -e 'p'`
title2=`findLine pdftitle =  $output | sed -ne 's/pdftitle=//g' -ne 's/{//g' -ne 's/}//g' -ne 's/,//g' -e 'p'`



Comment $output "% Title" "Body" '%'
Comment $output "Title" "Body" '%'
#cat  $output 

#cat $output | sed -e '/Title Data/,/Body/d' >  ttmp.txt
#mv ttmp.txt $output


#[[ -z "${rst_author}" ]] && rst_author="\\\[1em] I. Marfin \\\[1em] {\scriptsize on behalf of the DESY Higgs analysis group}"
 [[ -z "${rst_author}" ]] && rst_author="\\\[1em] I. Marfin "
 [[ -z "${rst_short_author}" ]] && rst_short_author="I. Marfin "
 [[ -z "${rst_short_institute}" ]] && rst_short_institute="DESY"


[[ -z  "${rst_additional_text}" ]] && rst_additional_text="DESY CMS   \\\[0.25em]  Higgs Group meeting "

[[  "${rst_logo}" = "yes" ]] && rst_logo_png="
% logo of my university
\titlegraphic{\includegraphics[width=1.5cm]{/home/school/rst2pdf-0.92/CMS_logo_col.png}\hspace*{4.75cm}~%
  \includegraphics[width=1.5cm]{/home/school/rst2pdf-0.92/Desy-logo-big.png}
}
"

rst_logo_png_add=""
[[  -n "${rst_additional_logo}"  ]] &&  rst_logo_png_add="

% 
\titlegraphic{\includegraphics[width=1.5cm]{/home/school/rst2pdf-0.92/CMS_logo_col.png}\hspace*{10pt}
 \includegraphics[width=8cm, height=1.5cm]{$rst_additional_logo}
%  \includegraphics[width=1.5cm]{/home/school/rst2pdf-0.92/comb_logo.png}
  \hspace*{10pt}\includegraphics[width=1.5cm, height=1.5cm]{/home/school/rst2pdf-0.92/Desy-logo-big.png}
}
"

[[  "${rst_background}" = "yes" ]] && rst_background_png="



\usepackage{tikz}

% taken from  http://tex.stackexchange.com/questions/74038/transparent-image-background-in-beamer

\usebackgroundtemplate{%
\tikz\node[opacity=0.2] {\includegraphics[height=\paperheight,widht=\paperwidth]{/home/school/rst2pdf-0.92/cms_background.png}}; }

"

[[ ! "${rst_background}" = "yes" ]] && rst_background_png=" "



[[  "${rst_short_logo}" = "yes" ]] && rst_short_logo_png="
% logo of my university
% taken from  http://tex.stackexchange.com/questions/27906/positioning-logo-in-the-front-page-as-well-as-slides
%\logo{\includegraphics[height=0.8cm]{logo.eps}\vspace{220pt}}

%\logo{
%\vspace{200pt}
%\includegraphics[width=0.8cm]{/home/school/rst2pdf-0.92/CMS_logo_small.png}\hspace*{0.5cm}~%
%\includegraphics[width=0.8cm]{/home/school/rst2pdf-0.92/DESY-Logo-small.jpg}
%}

\addtobeamertemplate{frametitle}{}{%
\begin{textblock*}{120mm}(.85\textwidth,-0.5cm)
\includegraphics[height=0.8cm,width=0.8cm]{/home/school/rst2pdf-0.92/DESY-Logo-small.png}\hspace*{0.5mm}
%\includegraphics[height=0.5cm,width=0.7cm]{/home/school/rst2pdf-0.92/ncphep-logo1.png} \hspace*{0.5mm}
\includegraphics[height=0.8cm,width=0.8cm]{/home/school/rst2pdf-0.92/CMS-Logo-small.png}
\end{textblock*}}

"

[[  "${rst_short_logo}" = "yes" ]] && rst_short_logo_header="\usepackage{textpos}"
[[ ! "${rst_short_logo}" = "yes" ]] && rst_short_logo_header="\usepackage{textpos}"
[[ ! "${rst_short_logo}" = "yes" ]] && rst_short_logo_header=" "
[[ ! "${rst_short_logo}" = "yes" ]] && rst_short_logo_png=" "

[[  ! "${rst_logo}" = "yes" ]] && rst_logo_png=" "

	
[[ -z "${rst_institute}" ]] && rst_institute="  \\\[1em]   \Large  DESY   \\\[1em] "


mytitlenew="\title[${rst_short_title}]$title3"

[  "${rst_nice_footer}" = "yes" ] &&  mytitlenew=" \title[{\makebox[.45\paperwidth]{${title2}\hfill\insertframenumber/\inserttotalframenumber}}]{${title2}} "

#%$mytitle



echo "

${rst_short_logo_header}
${rst_background_png}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{verbatim}
\usetikzlibrary{arrows,shapes}
\usetikzlibrary{shapes,backgrounds,fit,positioning}
\usepackage[makeroom]{cancel}
\usepackage{MnSymbol}
%\usepackage{stackengine}

$mytitlenew


%\title[${rst_short_title}]$title3 
%\title[{\makebox[.45\paperwidth]{${title2}\hfill%
%\insertframenumber/\inserttotalframenumber}}]${title2} 
%\title[{\makebox[.33\paperwidth]{\usebeamerfont{date in head/foot}\insertshortdate{}} \makebox[.33\paperwidth]{\usebeamerfont{author in head/foot}\insertshortauthor/\insertshortinstitute{}\hfill} \makebox[.33\paperwidth]{${title2}\hfill}}]{${title2}} 
%\title[{\makebox[.33\paperwidth]{\usebeamerfont{date in head/foot}\insertshortdate{}}  \makebox[.33\paperwidth]{${title2}\hfill}}]{${title2}} 

\author[${rst_short_author}]{ ${rst_author} }
\institute[${rst_short_institute}]{   ${rst_institute}  }


${rst_logo_png}
${rst_logo_png_add}

%\date{\scriptsize \today}
${rst_date} 
%\useoutertheme{infolines}


%_MARKER_BEGIN
" > ttttmp.txt
#cat ttttmp.txt
sed -e "/Title Data/r ttttmp.txt" $output > tttmp.txt
#sed -e "/%_MARKER_BEGIN/r ttttmp.txt" $output > tttmp.txt
#cat tttmp.txt

echo "


\begingroup
\usebackgroundtemplate{% 
}
\setbeamertemplate{footline}{\vspace*{-3em}\centering ${rst_additional_text} \vspace{2em} \par}
\setbeamertemplate{headline}{} 

%\begin{frame}[plain]
\begin{frame}
 \titlepage
\end{frame}
\endgroup

${rst_short_logo_png}


"  > ttttmp2.txt

if [  `echo "$*" | egrep -c ".*stylesheet.*" ` -eq 1 -a ${rst_toc} = "table" ]
then
echo "

\begin{frame}{Outline}
%\setcounter{collectmore}{-1}
\setcounter{unbalance}{2}

\raggedcolumns
\begin{multicols}{2}
\pdfbookmark[0]{Contents}{toc}
\tableofcontents{}
\end{multicols} 
\end{frame}

" >> ttttmp2.txt

else

echo "

\begin{frame}{Outline}
\pdfbookmark[0]{Contents}{toc}
\tableofcontents{}
\end{frame}

" >> ttttmp2.txt


fi

#Comment tttmp.txt  "maket" "itle" '%'


line3=`findLineNumber "make" "title" tttmp.txt`
echo "line3 = $line3"
sed -e "$((line3+1)),$((line3+12))d" tttmp.txt >  $output


sed -e "/maketitle/r ttttmp2.txt" $output > tttmp2.txt
sed -e "${line3}d" tttmp2.txt >  $output
#cat tttmp.txt 
#cp tttmp2.txt tttmp2.log

#mv tttmp2.txt $output

#fix titles and frametitles  \_h\textasciicircum{}

cat $output |   sed -ne '/\\.*title/s/\\\$/$/g' -ne '/\\.*title/s/\\textbackslash{}/\\/g' -ne '/\\.*title/s/\\_/_/g' -ne '/\\.*title/s/\\textasciicircum{}/\^/g' -ne '/\\.*title/s/\\{/{/g' -ne  '/\\.*title/s/\\}/}/g' -e 'p' > tttmp2.txt
mv tttmp2.txt $output


rm  ttmp.txt tttmp2.txt  tttmp.txt  ttttmp2.txt  ttttmp.txt



fi






[ -n "$dolatex" ] && latex  $output ;

echo  "$dodvipdf"

[  `ls ${file_name}.dvi` ] && [ -n "$dodvipdf" ] && dvipdf "${file_name}.dvi";

[ -n "$dopdflatex" ] && pdflatex $output

	 
#run_rst2beamer_end_
}


prepare_rst2pdf_template()
{
#prepare_rst2pdf_template_begin_

local help='

run program rst2pdf to create presentation

\$1 how many plots per slides
\$2 default size of plot
\$3 where to look for plots
\$4 what extenstion use for plots: jpg, png
\$5 position of figures
\$6 title

Example:
  prepare_rst2pdf_template 4 "300:400" "" eps 8 "Some plots" > test4.rst
  run_rst2pdf `pwd`/test4.rst
  evince bruce-output/test4.rst.pdf


if defined variable "relpath=yes", then paths will be relative (\$3/image.png) in image::  directives

 prepare_rst2pdf_template 3 "400:600" "" eps 8 "Some plots" > test5.rst
 run_rst2pdf `pwd`/test5.rst
 evince bruce-output/test5.rst.pdf

'
if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ "$#" -lt 1 ]; then print_help "$0" "$help"; return 1; fi;




local _many=1;
local _size=700;
local _sizeW=$_size;

if [ -n "$1" ]; then _many=$1; fi

if [ $_many -eq 2 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=600
                _sizeW=800
	fi
	if [ $_many -eq 3 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=400
                _sizeW=500
	fi

	if [ $_many -eq 4 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=380
                _sizeW=600
	fi

if [ -n "$2" ] 
then 
	_size=`echo $2 | awk -F':'  '{print $1}'`
    _sizeW=`echo $2 | awk -F':' '{print $2}'`

fi 

local position=8
[[ -n "$5" ]] && position="$5" 

local title="Some title"
[[ -n "$6" ]] && title="$6" 


echo "

.. role:: raw-math(raw)
    :format: latex html

"

where="./"
if [ -n "$3" ]; then where="$3"; fi

if [ -n "$4" ]; then  ext="$4"; else ext="jpg"; fi

if [ $relpath == "yes" ]
then
find $where -iname "*.$ext" | grep -v "legend"   | uniq | xargs -I {} echo " echo -n \".. |{}| image:: \";  echo {}; echo \"            :width: $_sizeW
            :height: $_size

\"  " |sh


else
find `pwd` -iname "*.$ext" | grep -v "legend" | grep "$where"  | uniq | xargs -I {} echo " echo -n \".. |{}| image:: \";  echo {}; echo \"            :width: $_sizeW
            :height: $_size

\"  " |sh

fi

if [ $relpath == "yes" ]
then
a=($(find $where -iname "*.$ext" | grep -v "legend"     | sort -r  ));
else 
a=($(find `pwd` -iname "*.$ext" | grep -v "legend"   | grep "$where"  | sort -r  ));
fi

#echo a "${a[0]} ${a[1]}"

#a=($(echo $a | tr ' ' '\n'));

#echo a "${a[0]} ${a[1]}"

#echo "a = $a "

len=`echo $title | wc -c`
local title2="=";
for ((i=1;i<$len;i++)); do title2=`echo ${title2}=`;done



echo " "
echo $title2
echo $title
echo $title2
echo "|
|
|
"
echo ":author:	\"I. Marfin <igor.marfin@desy.de>\""
echo " "
echo "|
|
|
"
echo ".. image::  /home/school/rst2pdf-0.92/Desy-logo-big.gif
        :width: 200
        :height: 200
"

local startpos=0;

[[ -n "${a[0]}"  ]] &&  startpos=1;

#echo "startpos=$startpos"

if [ $_many -eq 2 ]
then

c=`echo "scale=0; ${#a[@]}/2"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*2"|bc`

#echo $c
#echo $c2
#i=1
#echo ${a[$((i-$startpos))]}





for ((i=1;i<=$((c*2));i+=2 ))
do

# determine the len of caption
local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done



echo "
.. raw:: pdf

      PageBreak


"

echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} |  sed -ne 's/_can_//g'     -ne  "s/.$ext//g"   -ne 's/_0//g' -e 'p' `; [[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+1-$startpos))]} | sed -ne 's/_can_//g' -e 'p' | sed -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
#echo "----------------------"
echo $capt
for (( k=1; k<=$position; k++ ))
do
echo "|"
done
#echo "
#|
#|
#|
#|
#|
#|
#|
#|
#
#"
echo " "
echo -n "|${a[$((i-$startpos))]}| "
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo "|${a[$((i+1-$startpos))]}|"
echo 
echo
done



if [ $c2 -gt 0 ] 
then 
echo "
.. raw:: pdf

      PageBreak


"

 (echo -n "Figure: "; echo -n  `basename ${a[$((c*2+1-$startpos))]} | sed -ne 's/_can_//g' -e 'p' | sed -ne "s/_0//g" -ne ".$ext//p" -e 'p'`); 
echo " "
local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done
#echo "----------------------"
echo $capt

for ((k=1;k<=$position;k++))
do
echo "|"
done
echo " "
#echo "
#|
#|
#|
#|
#|
#|
#|
#|
#
#"
 echo  "|${a[$((c*2+1-$startpos))]}|"

echo
echo
fi

fi

if [ $_many -eq 3 ]
then

c=`echo "scale=0; ${#a[@]}/3"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*3"|bc`

#echo "c=$c"
#echo "c2=$c2"

for ((i=1;i<=$((c*3));i+=3 ))
do
local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done

echo "
.. raw:: pdf

      PageBreak


"


echo -n "Figure: "; echo -n  `basename ${a[$((i - $startpos)) ]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2 -$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+2-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
#echo "----------------------"
echo $capt
for (( k=1;k<=$position;k++ ))
do
echo "|"
done
echo " "
#echo "
#
#|
#|
#|
#|
#|
#|
#
#
#"

echo -n "|${a[$((i-$startpos))]}| " 
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo -n "|${a[$((i+1-$startpos))]}| "
[[ -n "${a[$((i+2-$startpos))]}" ]] && echo -n "|${a[$((i+2-$startpos))]}|"

echo 
echo
echo
	
done



if [ $c2 -gt 0 ]
then

local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done

echo "
.. raw:: pdf

      PageBreak


"


(echo -n "Figure: ";  echo -n  `basename ${a[$((c*3+1-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p' `); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*3+2-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
echo " "
#echo "----------------------"
echo $capt
for ((k=1;k<=$position;k++))
do
echo "|"
done

echo " "
#echo "
#
#|
#|
#|
#|
#|
#|
#
#"
[[ $c2 -gt 0 ]] && echo -n "|${a[$((c*3+1-$startpos))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2-$startpos))]}" ]] && echo -n " |${a[$((c*3+2-$startpos))]}|"

echo
echo

fi
fi


if [ $_many -eq 4 ]
then
c=`echo "scale=0; ${#a[@]}/4"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*4"|bc`

for ((i=1;i<=$((c*4));i+=4 ))
do

local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done


echo "
.. raw:: pdf

      PageBreak


"

echo -n "Figure: "; echo -n  `basename ${a[$((i-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`; 
[[ -n "${a[$((i+1-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ -n "${a[$((i+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+2-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
[[ -n "${a[$((i+3-$startpos))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+3-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
#echo "----------------------"
echo $capt
local position2=`echo "scale=0;$position/2"|bc`;
for (( k=1;k<=$position2;k++ ))
do
echo "|"
done
echo " "
#echo "
#
#|
#|
#|
#|
#|
#|
#
#"
echo -n "|${a[$((i-$startpos))]}|" 
[[ -n "${a[$((i+1-$startpos))]}" ]] && echo " |${a[$((i+1-$startpos))]}|"

echo " "


local position2=`echo "scale=0;$position/2"|bc`;
[[ -n "${a[$((i+2-$startpos))]}" ]] &&  for (( k=1;k<=$position2;k++ ))
do
echo "|"
done
echo " "

#echo " 
#|
#|
#|
#|
#|
#|
#" 
echo -n "|${a[$((i+2-$startpos))]}| "
[[ -n "${a[$((i+3-$startpos))]}" ]] && echo  " |${a[$((i+2-$startpos))]}|"

echo 
echo
done

if  [ $c2 -gt 0 ]
then 
local capt="-";
for ((j=1;j<400;j++)); do capt=`echo ${capt}-`;done

echo "
.. raw:: pdf

      PageBreak


"


(echo -n "Figure: " ;  echo -n  `basename ${a[$((c*4+1-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne  "s/.$ext//g" -e 'p'`); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2-$startpos))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*4+2-$startpos))]} | sed -ne 's/_can_//g' -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`);
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3-$startpos))]}" ]] && (echo -n "  ";   echo -n  `basename ${a[$((c*4+3-$startpos))]} | sed -ne 's/_can_//g'  -ne "s/_0//g" -ne "s/.$ext//g" -e 'p'`)
echo " "
#echo "----------------------"
echo $capt
#echo "
#
#|
#|
#|
#|
#|
#|
#
#"
local position2=`echo "scale=0;$position/2"|bc`;
for (( k=1;k<=$position2;k++ ))
do
echo "|"
done
echo " "



[[ $c2 -gt 0 ]] && echo -n  "|${a[$((c*4+1-$startpos))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2-$startpos))]}" ]] && echo " |${a[$((c*4+2-$startpos))]}|"

echo " "

local position2=`echo "scale=0;$position/2"|bc`;
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3-$startpos))]}" ]] &&   for (( k=1;k<=$position2;k++ ))
do
echo "|"
done
echo " "

#echo "  
#|
#|
#|
#|
#|
#|
#
#" && 

echo "|${a[$((c*4+3-$startpos))]}|"

echo
echo
fi
fi


title_short=(`echo $title`)


echo "
.. footer::

            I. Marfin   |   `date +%D`  |  ${title_short[0]} ${title_short[1]}  ${title_short[2]}   |   Slide       ###Page###     
"

#           I. Marfin   |   `date +%D`  |   ###Title###   |   Slide       ###Page###

#echo "
#   .. class:: footertable 
#
#      +---+--------------------------------+-------------------+
#      |   |.. class:: left                 |.. class:: left    |
#      |   |                                |                   |
#      |   |I. Marfin `date +%D` ###Title###|Slide /###Page###/ |
#      +---+--------------------------------+-------------------+
#
#"
#           I. Marfin  `date +%D`  ###Title###             ###Page###



#prepare_rst2pdf_template_end_
}

run_rst2pdf()
{
#run_rst2pdf_begin_
local help='

run program rst2pdf to create presentation

#$1 -- file to be processed in full path
#$2, $3 etc -- options of rst2pdf. --help describes all of them


'



if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

local file_name="$1"

#ls -d `dirname $file_name`/bruce-output >& /dev/null  && 
mkdir -p  `dirname $file_name`/bruce-output;

local style="/home/school/rst2pdf-0.92/slides.style"
local rst2pdf="/usr/local/bin/rst2pdf";
local output=`dirname $file_name`/bruce-output/`basename $file_name`.pdf;
shift;
echo "$rst2pdf   $*  -b1 -s slides.style $file_name -o $output"
$rst2pdf   $*  -b1 -s $style $file_name -o $output

	 
#run_rst2pdf_end_
}


prepare_presentation_area()
{
#prepare_presentation_area_begin_
local help='

create folder and file .rst for presentation

#$1 -- title of presentation to be put in file name
#$2  -- suffix of the presentation folder

#$2 can be "higgsDesy" or "BTV" or "TSG".

Example how to reduce the number of slides. 
It seems that there is a maximum around ~85 slides

cat example.rst| xargs -I {} echo " [[ \"{}\" = \"Figure\" ]] && a=\$((a+1)); [[ \$a -lt 82 ]] && echo \"{} \"; [[ \$a -ge 82  ]] &&  if [[  \"{}\" = \"Figure\" ||  \"{}\" = \"------\" ]]; then b=1 ;else echo \"{}\" ; fi ;  echo  " |sh
 
'

local title=`echo $@ | tr ' ' '_'`;


if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

suffix="higgsDesy"

[ $# -gt 1 ] &&  [ -n "$2" ]  &&  suffix=$2

 mkdir `presentation_name $suffix`;
 cd `presentation_name $suffix`;
 touch `presentation_name "marfin_$title.rst"`;
 cd -;
#prepare_presentation_area_end_
}

prepare_bruce_template()
{
#prepare_bruce_template_begin_

local help='

run program bruce to create presentation

'





echo ".. style::
   :layout.valign: center
   :align: center
   :font_size: 14

.. resource:: /usr1/scratch/marfin/presentations/

"

find `pwd` -iname "*jpg" -exec dirname {} \; | uniq | xargs -I {} echo ".. resource:: {}"

echo "

.. layout::
   bgcolor: silver
   image:Desy-logo-small50x50.png;halign=right;valign=top
   quad:C#ffc0a0;V0,h;V0,h-5*14;Vw,h-5*14;Vw,h
   viewport:0,2*14,w,h-5*14
   quad:C#ffc0a0;V0,2*14;V0,0;Vw,0;Vw,2*14



.. style::
        :layout.valign: center
        :align: left
        :font_size: 14
        :default.font_name: Times New Roma

"


echo ".. footer::
               I. Marfin   Some Text here \t\t\t `date \"+%B %d, %Y\"`

"

local _many=1;
local _size=700;
local _sizeW=$_size;

if [ -n "$1" ]; then _many=$1; fi
if [ -n "$2" ]; 
then 
	_size=$2
        _sizeW=$_size
else 
	if [ $_many -eq 2 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=450
                _sizeW=450
	fi
	if [ $_many -eq 3 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=450
                _sizeW=450
	fi

	if [ $_many -eq 4 ]
	then
	#	_size=`echo "scale=0; $_size/$1"|bc` 
		_size=400
                _sizeW=400
	fi
fi


#find `pwd` -iname "*jpg" -exec basename {} \; | uniq | xargs -I {} echo ".. |{}| image:: {}
#            :width: 400
#            :height: 400 
#
#"

find `pwd` -iname "*jpg"  | uniq | xargs -I {} echo " echo -n \".. |{}| image:: \";  basename {}; echo \"            :width: $_sizeW
            :height: $_size

\"  " |sh


# find `pwd` -iname "*jpg" -exec basename {} \; | uniq | xargs -I {} echo "Figure
#------
#
#- |{}|
#
#"


a=($(find `pwd` -iname "*jpg"));
a=($(echo $a | tr ' ' '\n'));


if [ $_many -eq 2 ]
then

c=`echo "scale=0; ${#a[@]}/2"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*2"|bc`


for ((i=1;i<=$((c*2));i+=2 ))
do
echo ".. style:: :title.position:  w/2,h-14 "
echo -n "Figure: "; echo -n  `basename ${a[$i]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`; [[ -n "${a[$((i+1))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`)
echo "------"
echo
echo "- |${a[$i]}|"
[[ -n "${a[$((i+1))]}" ]] && echo "- |${a[$((i+1))]}|"
echo 
done

echo "
.. style::
        :title.position:  w/2,h-14
"
[[ $c2 -gt 0 ]] && (echo -n "Figure: "; echo -n  `basename ${a[$((c*2+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`); 
echo " "
echo "------"
echo
[[ $c2 -gt 0 ]] && echo  "- |${a[$((c*2+1))]}|"

echo



fi

if [ $_many -eq 3 ]
then

c=`echo "scale=0; ${#a[@]}/3"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*3"|bc`

#echo "c=$c"
#echo "c2=$c2"

for ((i=1;i<=$((c*3));i+=3 ))
do
echo ".. style:: :title.position:  w/2,h-14 "
echo -n "Figure: "; echo -n  `basename ${a[$i]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`; 
[[ -n "${a[$((i+1))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`);
[[ -n "${a[$((i+2))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+2))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`)
echo "------"
echo
echo "|${a[$i]}|" 
[[ -n "${a[$((i+1))]}" ]] && echo "|${a[$((i+1))]}|"
[[ -n "${a[$((i+2))]}" ]] && echo "|${a[$((i+2))]}|"

echo 
done


echo "
.. style::
        :title.position:  w/2,h-14
"

[[ $c2 -gt 0 ]] && (echo -n "Figure: ";  echo -n  `basename ${a[$((c*3+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*3+2))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`);
echo " "
echo "------"
echo
[[ $c2 -gt 0 ]] && echo -n "- |${a[$((c*3+1))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*3+2))]}" ]] && echo -n " |${a[$((c*3+2))]}|"

echo



fi

if [ $_many -eq 4 ]
then
c=`echo "scale=0; ${#a[@]}/4"|bc`
c2=`echo "scale=0;  ${#a[@]}-$c*4"|bc`

for ((i=1;i<=$((c*4));i+=4 ))
do
echo "
.. style:: 
	:title.position:  w/2,h-14
	:title.font_size: 15 
"
echo -n "Figure: "; echo -n  `basename ${a[$i]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`; 
[[ -n "${a[$((i+1))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`);
[[ -n "${a[$((i+2))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((i+2))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`)
[[ -n "${a[$((i+3))]}" ]] && (echo -n "  ";   echo   `basename ${a[$((i+3))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`)
echo "------"
echo
echo -n "- |${a[$i]}|" 
[[ -n "${a[$((i+1))]}" ]] && echo " |${a[$((i+1))]}|"
[[ -n "${a[$((i+2))]}" ]] && echo -n "- |${a[$((i+2))]}|"
[[ -n "${a[$((i+3))]}" ]] && echo  " |${a[$((i+2))]}|"

echo 
done

echo "
.. style::
        :title.position:  w/2,h-14
        :title.font_size: 15
"
[[ $c2 -gt 0 ]] && (echo -n "Figure: " ;  echo -n  `basename ${a[$((c*4+1))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`); 
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2))]}" ]] && (echo -n "  ";   echo -n   `basename ${a[$((c*4+2))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`);
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3))]}" ]] && (echo -n "  ";   echo -n  `basename ${a[$((c*4+3))]} | sed -ne 's/_can_//p' | sed -ne 's/_0.jpg//p'`)
echo " "
echo "------"
echo
[[ $c2 -gt 0 ]] && echo -n "- |${a[$((c*4+1))]}|"
[[ $((c2 -1)) -gt 0 ]] && [[ -n "${a[$((c*4+2))]}" ]] && echo " |${a[$((c*4+2))]}|"
[[ $((c2 -2)) -gt 0 ]] && [[ -n "${a[$((c*4+3))]}" ]] && echo -n "- |${a[$((c*4+3))]}|"

echo


fi

#prepare_bruce_template_end_
}


run_bruce()
{
#run_bruce_begin_
local help='

run program bruce to create presentation

#$1 -- file to be processed in full path
#$2, $3 etc -- options of bruce. --help describes all of them

Here is an example of bruce presentation:

.. style::
   :layout.valign: center
   :align: center
   :font_size: 30

.. resource:: /usr1/scratch/marfin/presentations/
.. resource:: /usr1/scratch/marfin/presentations/21.03.12_higgsDesy/plots

.. layout::
   bgcolor: silver
   image:Desy-logo-small.gif;halign=right;valign=top
   quad:C#ffc0a0;V0,h;V0,h-88;Vw,h-88;Vw,h
   viewport:0,64,w,h-(64+48)


.. style::
        :layout.valign: center
        :align: left
        :font_size: 18
        :default.font_name: Times New Roman



.. |img1| image:: _can_Et2byEt1_0.jpg
        :width: 400
        :height: 400
.. |img2| image:: _can_Et3byEt1_0.jpg
        :width: 400
        :height: 400

.. footer::
        I. Marfin TMVA input

Collection of plots
------


Flavor content

- |img1|  |img2|
- |img1|  |img2|


Example how to reduce the number of slides.
It seems that there is a maximum around ~10 slides

cat example.rst| xargs -I {} echo " [[ \"{}\" = \"Figure\" ]] && a=\$((a+1)); [[ \$a -lt 10 ]] && echo \"{} \"; [[ \$a -ge 10  ]] &&  if [[  \"{}\" = \"Figure\" ||  \"{}\" = \"------\" ]]; then b=1 ;else echo \"{}\" ; fi ;  echo  " |sh



'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

local file_name="$1"
local bruce="/usr1/scratch/marfin/presentations/bruce-3.2.1/bruce.sh";
local output="`dirname $file_name`/bruce-output/`basename $file_name`";
cd `dirname $bruce`;
shift;
$bruce  --record=$output -p --window-size=1200x800 $*  $file_name;
#$bruce  -p --window-size=1200x800 $*  $file_name ;
cd -;
 
#run_bruce_end_
}

test2()
{
#test2_begin_
local help='

NO HELP

FOR TEST PURPOSE
'

if [ "$1" = "help" ]; then print_help "$0" "$help"; return 1; fi;
if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

local filen=$1;

if [ "$1" = `basename $1` ]; then filen=`pwd`/$1;fi;


find `pwd` -type d -iname "*" -exec sh -c "echo \$1 ; cd \$1 ; $filen 2>/dev/null; cd - &> /dev/null;" _ {} \;

#local aaa="";
#local cmd="ls";
#local bbb="test2(){  ls -d */ | xargs -I {} echo \" echo {}; cd \`pwd\`/{}; $cmd; cd -; \" | sh }";
#bbb="test2(){  ls -d */ | xargs -I {} echo \" $bbb; echo {}; cd \`pwd\`/{}; $cmd; test2; cd -; \" | sh }";

#aaa="ls -d */ | xargs -I {} echo \"  echo \`pwd\`/{} ; cd \`pwd\`/{}; test2; cd -; \" "

#a="/usr1/scratch/marfin/presentations/utils.sh";

#echo $bbb

#ls -d */ | xargs -I {} echo "echo \`pwd\`/{} ; cd \`pwd\`/{};  cd -; " |sh;
#ls -d */ | xargs -I {} echo "source $a; echo \`pwd\`/{} ; cd \`pwd\`/{}; test2; cd -; " |sh
#ls -d */ | xargs -I {} echo "$bbb; echo \`pwd\`/{} ; cd \`pwd\`/{}; test2; cd -; " |sh
#test2_end_
}

apply_command()
{
#apply_command_begin_

local help='

apply command to all subfolders

#$1 -- command file

Example how to use it:

echo "rm *jpg"> mycmd; chmod a+x mycmd; apply_command mycmd; rm mycmd;
echo "rm *jpg; rm *png">| mycmd; chmod a+x mycmd; apply_command mycmd; rm  mycmd;
echo `pwd`"/eps2pdf.sh">| mycmd; chmod a+x mycmd; apply_command mycmd "*SgnBkg_v2_nPV*"; rm mycmd;
'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;


#local a;
#a=`find \`pwd\` -name $1`;
#[[ -n $a ]] &&  ls -d */ | xargs -I {} echo "echo {} ; cd {}; $a; cd - >& /dev/null" | sh


local filen=$1;
if [ "$1" = `basename $1` ]; then filen=`pwd`/$1;fi;

if [ -n "$2" ];
then 
	find `pwd` -type d -iname "$2" -exec sh -c "echo \$1 ; cd \$1 ; $filen 2>/dev/null; cd - &> /dev/null;" _ {} \;
else
	find `pwd` -type d -iname "*" -exec sh -c "echo \$1 ; cd \$1 ; $filen 2>/dev/null; cd - &> /dev/null;" _ {} \;

fi



#apply_command_end_
}


presentation_name()
{
#presentation_name_begin_
local help="

get valid name for presentation folder 

"

if [ "$1" = "help" ]; then  print_help "$0" "$help"; return 1; fi;

local a;
a=${1+"$@"};


echo `date +"%d.%m.%y_$a"`;
#presentation_name_end_
}


dirs_sizes()
{
#dirs_sizes_begin_
local help="

print the usage of all sub-folders 
if you put option -h it will give you output in human-readable format

you can estimate total size in MB:

dirs_sizes  | awk 'BEGIN{aaa=0;} {aaa+=\$1;} END{print aaa/1024 \"M\"}'

"

if [ "$1" = "help" ]; then  print_help "$0" "$help"; return 1; fi;

ls -d */ | xargs -I {} find `pwd`/{} -type d -iname "*" | xargs -I {} echo " echo {} | du $1  " |sh
#dirs_sizes_end_
}

help() 
{ 
#help_begin_
local help='

print information message codded in the format
///usage: my message
///usage: here
etc

#$1 -- file with help 

'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi;

grep  --context=2  usage $1 |  sed  -ne 's/\/\/\/usage://p' 
#help_end_
}

update()
{
#update_begin_
local help='

copy recursively some file to sub-folders
///usage: my help 
///usage: do it in the way
etc

#$1 -- file 
'

   if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi


#ls -d */ | xargs -I {} find `pwd`/{} -name $1 | xargs -I {} echo " echo updating {}; bbb1=\` stat -c %Y $1 \`; bbb2=\` stat -c %Y {} \`; bbb3=\$(( \$bbb1 - \$bbb2 )); echo \" \$bbb1 \$bbb2 \$bbb3 \";  if [ \$bbb3 -lt 0 ]; then echo \"1\" >| aaa;  echo 222; else echo 111; fi;" | sh
#ls -d */ | xargs -I {} find `pwd`/{} -name $1 | xargs -I {} echo " echo -n \" updating {}     \"; bbb1=\` stat -c %Y $1 \`; bbb2=\` stat -c %Y {} \`; bbb3=\$(( \$bbb1 - \$bbb2 ));    if [ \$bbb3 -lt 0 ]; then echo \"1\" >| aaa;  echo \"   |  copying back   \";  cp {}  \`pwd\`/$1;  else echo \"   |  copying forward   \"; cp \`pwd\`/$1 {}; fi;" | sh
ls -d */ | xargs -I {} find `pwd`/{} -name $1 | xargs -I {} echo " printf \" %-100s \" \" updating {}     \"; bbb1=\` stat -c %Y $1 \`; bbb2=\` stat -c %Y {} \`; bbb3=\$(( \$bbb1 - \$bbb2 ));    if [ \$bbb3 -lt 0 ]; then echo \"1\" >| aaa;   printf \" %20s \" \"   |  copying back  \"; echo ;  cp {}  \`pwd\`/$1;  else printf \" %20s \" \"   |  copying forward  \"; echo ;  cp \`pwd\`/$1 {}; fi;" | sh

#[[ -f aaa ]] && echo aaa=`cat aaa`;

# doesn't work in bash
#[[ -f aaa ]] && [[ `cat aaa` -gt 0 ]] && { rm aaa; echo;echo; update $1 }
# it works in bash
[[ -f aaa ]] && [[ `cat aaa` -gt 0 ]] && ( rm aaa; echo;echo; update $1 )
[[ -f aaa ]] && rm aaa;
#update_end_
}

ListXMLParamters()
{
#ListXMLParamters_begin_
local help='

return list of the parameters corresponded to the xml tag in the file:
#$1 -- xml file
#$2 -- tag
'
        if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi


# sed -n  -e ':a' -e  "s/\(.*\)$1\(.*\)$1\(.*\)/\2;;;\1/p;ta" 
# sed -n -e ':a' -e 's/\(.*\)\(PDFDescriptor\)\(.*\)=\( *\)\([^ ]*\)/\2 \3/p;ta'  TMVAClassification_LikelihoodKDE.weights.xml | grep -v "="
# sed -n -e ':a' -e 's/\(.*\)\(PDF\)\( \{1,\}\)\(.*\)=\( *\)\([^ ]*\)/\2\3\4/p;ta'  TMVAClassification_LikelihoodKDE.weights.xml | grep -v "="
# sed -n -e ':a' -e 's/\"[^\"]*\"/aaa/g;s/\(.*\)\(PDF\)\( \{1,\}\)\(.*\)=\( *\)\([^ ]*\)/\2\3\4/p;ta'  TMVAClassification_LikelihoodKDE.weights.xml | grep -v "="

local file=$1
local tag=$2;

res=`echo " sed -n -e \':a\' -e \'s/\"[^\\\\\\"]*\"/aaa/g;s/\(.*\)\($tag\)\( \{1,\}\)\(.*\)=\( *\)\([^ ]*\)/\2\3\4/p;ta\' $file  | grep -v \"=\" | sed -ne \'s/\(.*\)\($tag\)\(.*\)/\1\3/p\' " | sh`;

echo "$res"

return 0;
#ListXMLParamters_end_
}

ReadXMLParameter()
{
#ReadXMLParameter_begin_
local help='

return parameter of the xml tag in the file:
#$1 -- xml file
#$2 -- tag 
#$3 -- parameter name
'
        if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

local file=$1
local tag=$2;
local par=$3;

res=`echo " sed -ne 's/\(.*\)\($tag\)\(.*\)\($par\)\( *\)=\( *\)\([^ ]*\)\(.*\)/\7/p' $file " | sh`;

###remove all special symbols: ", >, <

echo "$res" | tr -d "\"" | tr -d ">" | tr -d "<";


return 0;

#ReadXMLParameter_end_
}



findLine()
{

#findLine_begin_

local help='

find a number of  line:
#$1 -- some word "A" in line
#$2 -- some word "B" in line
#$3 -- file name to be processed

cat $3 :

....
..... *.A.*B.*  --> line  will be printed
.....
.....
'
        if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi     
         sed -ne "/.*$1.*$2/p" $3


#findLine_end_

}

HowManyFieldsInFile()
{
#HowManyFieldsInFile_begin_

local help="
It tries to find the number of field in files having information to create objects of type:
	* runProof v2 (Sample)
	* runProof v3 (Sample)
	* Plotter
	* TMVA support
	* etc

Then file 'function.h' (or 'function_runProof.h', or similar) contains functions for reading files
	*Readinput_v2
	*Readinput_v3
	etc


#\$1 -- name of the file
"
 if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

	res=`cat $1 | awk 'END{print NF}'`;
	echo "$res";

#HowManyFieldsInFile_end_
}

SplitListOfFiles()
{
#SplitListOfFiles_begin_
local help="

	If we have a list of root ntuples to be processed, then we could want to produce smaller lists of files to submit each of them to batch.
	It allows to increase a speed of calculations.
	The field corressponded  to number of files is being changed.

	Files called \$1_number will be produced

#\$1 -- name of the file containing
#\$2 -- how many files must be inside new lists

"

 if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi



local content="content";
local startpos=1;
local endpos=`echo "$startpos + $2 -1" | bc `;
local i=0;
while [  "$content" != "" ]
do
	i=$(($i+1));
	content=`cat $1 | sed -ne "${startpos},${endpos}p"`;

#	echo $startpos, $endpos
#	echo 222 "$content" 111;


	startpos=`expr $endpos + 1`;
	endpos=`echo "  $startpos + $2 -1"|bc`;
	local linesNum=`echo "$content" | wc -l`;
#	echo $linesNum;
	content=`echo "$content" | awk "{ if (NR==1 && \\$1 !=\"\") \\$6=$linesNum; print \\$0}"`

if [  "$content" != "" ]; then echo "$content" > $1_$i; fi;

done




return 0;

#SplitListOfFiles_end_
}


ListOfFilesForCrab()
{
#ListOfFilesForCrab_begin_
local help="

	If we have a list of root ntuples to be processed, then we could want to produce smaller lists of files to submit each of them to crab

	Files called \$1_number will be produced

#\$1 -- name of the file containing
#\$2 -- job number
#\$3 -- how many jobs at all

"

 if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi


###Calculate delta: endpos = startpos + delta -1 
local numlines=`cat $1 | wc -l`;
local delta=`echo "$numlines/$3" | bc`;

if [ $delta -eq 0 ]; then delta=1; fi;

local content="content";
local startpos=`echo "($2-1)*$delta + 1" | bc`;
local endpos=`echo "$startpos + $delta -1" | bc `;
local limitpos=`echo "$delta*$3" | bc`;

content=`cat $1 | sed -ne "${startpos},${endpos}p"`;

if [ "$content" != "" ]
then
	 echo "$content" > $1_$2;
else
	echo " "> $1_$2;
	return 1;

fi

while [ "$endpos" -ge "$limitpos" -a  "$content" != "" ]
do

#	echo $startpos, $endpos
#	echo 222 "$content" 111;


	startpos=`expr $endpos + 1`;
	endpos=`echo "  $startpos + $delta -1"|bc`;

	content=`cat $1 | sed -ne "${startpos},${endpos}p"`;

#	local linesNum=`echo "$content" | wc -l`;
#	echo $linesNum;
#	content=`echo "$content" | awk "{ if (NR==1 && \\$1 !=\"\")  print \\$0}"`

if [  "$content" != "" ]; then echo "$content" >> $1_$2; fi;

done


return 0;

#ListOfFilesForCrab_end_
}



createCPPCode()
{
#createCPPCode_begin_

local help="
convert shell function to ROOT macros
#\$1 -- name of the function
"
 if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

args=`$1 | sed -ne 's/.*#$\(.*\).*/\1/p' | awk '{print $1}'`


if [ -f $1.C ]; then rm $1.C; fi;

touch $1.C;


argstr="";

#echo $args

##Forming call line of function
for i in $args
do
argstr=`echo "${argstr}TString arg$i,"`;
done

argstr=`echo "${argstr}TString pathArg=\"./\","`;
argstr=`echo "${argstr}Bool_t verbose=kFALSE,"`;


argstr=`echo "(${argstr%%,})"`;


headline=`echo "TString $1$argstr{"`;
#codeline1=($(echo "TString cmd=TString(\"$1 $argstr2 \");"));
#sections=($(FindSection "[" "]" "$1"));
unset codeline;
codeline[$((${#codeline[@]}+1))]=`echo "TString cmd=TString(\"source \");"`;
codeline[$((${#codeline[@]}+1))]=`echo " cmd+=pathArg+TString(\"utils.sh; \");"`;
codeline[$((${#codeline[@]}+1))]=`echo "cmd+=TString(\"$1 \" );"`;

for i in $args
do
codeline[$((${#codeline[@]}+1))]=`echo "cmd+=TString(' ')+arg$i+TString(' ');"`;
done



#codeline[$((${#codeline[@]}+1))]=`echo "FILE * myPipe = gSystem->OpenPipe(cmd.Data(),\"r\");"`;
codeline[$((${#codeline[@]}+1))]=`echo "TString res =gSystem->GetFromPipe(cmd.Data()) ;"`;
#codeline[$((${#codeline[@]}+1))]=`echo "res.Gets(myPipe);"`;
#codeline[$((${#codeline[@]}+1))]=`echo "gSystem->ClosePipe(myPipe);"`;




printline=`echo "if (verbose) std::cout<<res<<std::endl;"`;
returnline=`echo "return res;}"`;

###Printing to file
output="///run me"
echo $output >> $1.C;
output="///  $1.C$argstr";
echo $output >> $1.C;


echo >>$1.C
echo >>$1.C

echo "#include \"TString.h\"" >>$1.C
echo "#include \"TSystem.h\"" >>$1.C
echo "#include <iostream>" >>$1.C

echo >>$1.C
echo >>$1.C



echo $headline >>$1.C
for ((i=1;i<=${#codeline[@]};i++))
do
echo ${codeline[$i]} >>$1.C;
done
echo $printline >>$1.C
echo $returnline >>$1.C;


cat $1.C

return 0;

#createCPPCode_end_
}

FindSection()
{
#FindSection_begin_
local help="

find a text from one pos to another pos
It's useful to process multicrab.cfg having format :

[section1]

class1.atribute1=val11
class1.atribute2=val12
class1.atribute3=val13



[section2]

class2.atribute1=val21
class2.atribute2=val22
class2.atribute3=val23

etc


#\$1 -- pos1
#\$2 -- pos2
#\$3 -- file to  be processed
"

        if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

local pos1=$1;
local pos2=$2;
local file=$3;


cat $file | sed "/#/d" |  awk -v pos1=$pos1 -v pos2=$pos2  "BEGIN {c=0} { if( index(\$$0,pos1)>0) {c=NR } if(c>0) { print \$$0}  if (index(\$$0,pos2) && c>0)  {c=0} }" | tr -d "[" | tr -d "]" |  tr -d "#";


return 0;
#FindSection_end_
}

ProcessMulticrab_cfg()
{
#ProcessMulticrab_cfg_begin_
local help="


find all sections and fill assoc array in the format:
arr["section1_class1.atribute1"]=val11;
arr["section2_class2.atribute1"]=val21;
etc

Then *_job_config files are produced which might be used to run ProofCluster in the batch job

assocArray.sh must be in the same dir as utils.sh

#\$1 -- file to be processed, like multicrab.cfg
"

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

### Let's find assoc array support

removeAllElement
cc=`findElement 2>&1`;

b=`echo "$cc" |  awk '{if (index($0, command not found)>0) {print 1 }}'`;


if [ "$b" -eq 1 -a  -f  "assocArray.sh" ]
 then 
	echo "No assoc array support found. Try to ini it";
	source ./assocArray.sh;
	a=`findElement 2>&1`;
	else
	echo "No assocArray.sh. return";
	return 1;
fi




##Fill temporary array with section's names
sections=($(FindSection "[" "]" "$1"));
len=${#sections[@]};
#echo $len
 for ((j=1;j<=$len;j++))
                do
#		echo j=$j		
		beg=`findLineNumber "${sections[$j]}" "]" $1 |   tr " " "\n" | tail -1`;
		end=`findLineNumber "${sections[$((j+1))]}" "]" $1  | tr " " "\n" | tail -1`;

		echo "${sections[$((j))]}"
		echo "${sections[$((j+1))]}"
	
#		echo "beg=$beg";
#		echo "end=$end";

		if [ "$beg" = "$end" ]; then end=`cat $1 | wc -l`; fi;


		curtext=`sed -ne "$((beg+1)),$((end-1))p" < \`echo $1\``;


#removing comments with '#'
		 curtext=`echo $curtext |  sed "s/\ *#.*\ *//g"`;

#removing white space araound '='	
		curtext=`echo $curtext | sed  "s/ *= */=/g"`;
#		echo 11${curtext}22;

##Fill array!
		if [ -n "$curtext"  -a -n "${sections[$j]}" ]		
		then
			curtext2=($(echo "$curtext" | tr ' ' '\n' ));
			len2=${#curtext2[@]};
			for (( k=1;k<=$len2;k++ ))
			do
				elem=`echo "${curtext2[$k]}" | sed -ne "s/\(.*\)=\(.*\)/\1/p"`;
				val=`echo "${curtext2[$k]}" | sed -ne "s/\(.*\)=\(.*\)/\2/p"`;
				addElement "${sections[$j]}_$elem"  $val;
#				echo "adding ${sections[$j]}";
			done
		
		fi


		done




###List of available keywords
#USER.publish_data_name 
#CMSSW.datasetpath
#CMSSW.pset 
#CMSSW.total_number_of_events
#CMSSW.dbs_url 
##USER.type=SGN,BKG or DATA  # my new keywords!
##USER.weight=0.0025 #
#findElement "SingleTopBar_s_Trees_CMSSW.dbs_url"


###Produce pathes to files!
which voms-proxy-init >& /dev/null;
found=$?;
if [ "$found" -eq 1 ] ; then "ini GLITE32!"; return 1; fi;

len=${#sections[@]};
 for ((j=1;j<=$len;j++))
                do
		datasetpath=`findElement "${sections[$j]}_CMSSW.datasetpath"`;
		dbs_url=`findElement "${sections[$j]}_CMSSW.dbs_url"`;

		if [ -z "$dbs_url" ]; then  dbs_url="http://cmsdbsprod.cern.ch/cms_dbs_prod_global/servlet/DBSServlet";fi;
		
		if [ -n "$datasetpath" -a -n "$dbs_url" ]
		then

		echo files for ${sections[$j]}

##Find files
	files=`echo	"dbs search --url=\"$dbs_url\" --query=\"find file where dataset=$datasetpath\" | sed \"1,4d\""  | sh`;
		addElement "${sections[$j]}_files" "$files"
	       # files=($(echo "$files" | tr ' ' '\n' ));
	#	echo "${files[0]}"
		fi

		done



#final config_job file
if [ -f config_job ]; then rm job_config; fi;
touch job_config;

len=${#sections[@]};
for ((j=1;j<=$len;j++))
	do

		files=`findElement "${sections[$j]}_files"`;
		type=`findElement "${sections[$j]}_USER.type"`;
		weight=`findElement "${sections[$j]}_USER.weight"`;
		name=${sections[$j]};

		if [ -z "$name" ] ; then continue; fi;
		if [ -f ${name}_job_config ]; then rm ${name}_job_config; fi;
		touch ${name}_job_config;


		files=($(echo "$files" | tr ' ' '\n' ));
		files_len=${#files[@]};
		for ((k=1;k<=${files_len};k++))
		do
		output=`echo "dcap://dcache-cms-dcap.desy.de:22125//pnfs/desy.de/cms/tier2${files[$k]} $name $type"`;
#		echo "$output"
		if [ "$type" = "DATA" ]; then output=`echo "$output config_data.py" `; else output=`echo "$output config_mc.py"`; fi
		output=`echo "$output $weight"`;
		 if [ $k -eq 1 ]; then  output=`echo "$output $files_len"`; else output=`echo "$output 0"`; fi
		echo "$output" >> job_config;
		echo "$output" >> ${name}_job_config	
		done
	done



#findElement "SingleTopBar_s_Trees_USER.type"
#findElement "SingleTopBar_s_Trees_USER.weight"

return 0;
#ProcessMulticrab_cfg_end_
}

ProcessMulticrab_cfg_v2()
{
#ProcessMulticrab_cfg_v2_begin_
local help="


find all sections and fill assoc array in the format:
arr["section1_class1.atribute1"]=val11;
arr["section2_class2.atribute1"]=val21;
etc

Then *_job_config files are produced which might be used to run ProofCluster in the batch job

assocArray.sh must be in the same dir as utils.sh

Uses das_client.py. This file must be accesiable

#\$1 -- file to be processed, like multicrab.cfg

"

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

### Let's find assoc array support

removeAllElement
cc=`findElement 2>&1`;

b=`echo "$cc" |  awk '{if (index($0, command not found)>0) {print 1 }}'`;


if [ "$b" -eq 1 -a  -f  "assocArray.sh" ]
 then 
	echo "No assoc array support found. Try to ini it";
	source ./assocArray.sh;
	a=`findElement 2>&1`;
	else
	echo "No assocArray.sh. return";
	return 1;
fi

if [ ! -f "das_client.py" ]; then echo "No das_client.py found, return";   return 1; fi;



##Fill temporary array with section's names
sections=($(FindSection "[" "]" "$1"));
len=${#sections[@]};
#echo $len
 for ((j=1;j<=$len;j++))
                do
#		echo j=$j		
		beg=`findLineNumber "${sections[$j]}" "]" $1 |   tr " " "\n" | tail -1`;
		end=`findLineNumber "${sections[$((j+1))]}" "]" $1  | tr " " "\n" | tail -1`;

		echo "${sections[$((j))]}"
		echo "${sections[$((j+1))]}"
	
#		echo "beg=$beg";
#		echo "end=$end";

		if [ "$beg" = "$end" ]; then end=`cat $1 | wc -l`; fi;


		curtext=`sed -ne "$((beg+1)),$((end-1))p" < \`echo $1\``;


#removing comments with '#'
		 curtext=`echo $curtext |  sed "s/\ *#.*\ *//g"`;

#removing white space araound '='	
		curtext=`echo $curtext | sed  "s/ *= */=/g"`;
#		echo 11${curtext}22;

##Fill array!
		if [ -n "$curtext"  -a -n "${sections[$j]}" ]		
		then
			curtext2=($(echo "$curtext" | tr ' ' '\n' ));
			len2=${#curtext2[@]};
			for (( k=1;k<=$len2;k++ ))
			do
				elem=`echo "${curtext2[$k]}" | sed -ne "s/\(.*\)=\(.*\)/\1/p"`;
				val=`echo "${curtext2[$k]}" | sed -ne "s/\(.*\)=\(.*\)/\2/p"`;
				addElement "${sections[$j]}_$elem"  $val;
#				echo "adding ${sections[$j]}";
			done
		
		fi


		done




###List of available keywords
#USER.publish_data_name 
#CMSSW.datasetpath
#CMSSW.pset 
#CMSSW.total_number_of_events
#CMSSW.dbs_url 
##USER.type=SGN,BKG or DATA  # my new keywords!
##USER.weight=0.0025 #
#findElement "SingleTopBar_s_Trees_CMSSW.dbs_url"


###Produce pathes to files!
#which voms-proxy-init >& /dev/null;
#found=$?;
#if [ "$found" -eq 1 ] ; then "ini GLITE32!"; return 1; fi;

len=${#sections[@]};
 for ((j=1;j<=$len;j++))
                do
		datasetpath=`findElement "${sections[$j]}_CMSSW.datasetpath"`;
		dbs_url=`findElement "${sections[$j]}_CMSSW.dbs_url"`;

		if [ -z "$dbs_url" ]; then  dbs_url="http://cmsdbsprod.cern.ch/cms_dbs_prod_global/servlet/DBSServlet";fi;
		
		if [ -n "$datasetpath" -a -n "$dbs_url" ]
		then

		echo files for ${sections[$j]}

##Find files
	files=` ./das_client.py --query="file dataset=/2B2C1Jet_TuneZ2_7TeV-alpgen-pythia6/Summer11-PU_S4_START42_V11-v1/AODSIM | grep file.name" --format=plain --idx=0 --limit=100000 | awk '{if(NR>3) print \$0}'`
		addElement "${sections[$j]}_files" "$files"
	        # files=($(echo "$files" | tr ' ' '\n' ));
		# echo "${files[0]}"
		fi

		done



#final config_job file
if [ -f config_job ]; then rm job_config; fi;
touch job_config;

len=${#sections[@]};
for ((j=1;j<=$len;j++))
	do

		files=`findElement "${sections[$j]}_files"`;
		type=`findElement "${sections[$j]}_USER.type"`;
		weight=`findElement "${sections[$j]}_USER.weight"`;
		name=${sections[$j]};

		if [ -z "$name" ] ; then continue; fi;
		if [ -f ${name}_job_config ]; then rm ${name}_job_config; fi;
		touch ${name}_job_config;


		files=($(echo "$files" | tr ' ' '\n' ));
		files_len=${#files[@]};
		for ((k=1;k<=${files_len};k++))
		do
		output=`echo "dcap://dcache-cms-dcap.desy.de:22125//pnfs/desy.de/cms/tier2${files[$k]} $name $type"`;
		echo "$output"
		if [ "$type" = "DATA" ]; then output=`echo "$output config_data.py" `; else output=`echo "$output config_mc.py"`; fi
		output=`echo "$output $weight"`;
		 if [ $k -eq 1 ]; then  output=`echo "$output $files_len"`; else output=`echo "$output 0"`; fi
		echo "$output" >> job_config;
		echo "$output" >> ${name}_job_config	
		done
	done



#findElement "SingleTopBar_s_Trees_USER.type"
#findElement "SingleTopBar_s_Trees_USER.weight"

return 0;
#ProcessMulticrab_cfg_v2_end_
}



findLineNumber()
{
#findLineNumber_begin_
local help='

find a number of  line:
#$1 -- some word "A" in line
#$2 -- some word "B" in line
#$3 -- file name to be processed

cat $3 :

....
..... *.A.*B.*  --> line # will be printed
.....
.....
'
	if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi	
         sed -ne "/.*$1.*$2/=" $3
#findLineNumber_end_
}

print_all_functions()
{
#print_all_functions_begin_
local help='
print all functions in the file 

#$1
'

      if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

cat $1 |  sed -ne 's/\(.*\)()$/\1/p' | sed '/=/d' | sed '/#/d';

#print_all_functions_end_
}

print_all_functions_help()
{
#print_all_functions_help_begin_
local help='
print help of all functions in the file

#$1
'

      if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

a=`print_all_functions $1`;
a=($(echo $a | tr ' ' '\n'));
for ((i=1;i<=${#a[@]};i++))
do
	${a[$i]};

done

#print_all_functions_help_end_
}


field()
{
#field_begin_	

local help='
	from string 
	#$1 
	get field 
	#$2 
'


       if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

         echo "$1" | awk  "{print \$$2 }"

#field_end_

}

print_help()
{
#print_help_begin_
if [ $# -lt 2 ]; then return 1; fi

echo ""
echo "$1"
echo "usage:"
echo ""
echo "$2";

#print_help_end_
}

PowerToExp()
{
#PowerToExp_begin_

local help='
make transformation of the form from  0.012 to 1.2e-02
Current format is:
	4 -- number fields in mantisa

#$1 -- input number
'
        if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi


 echo "$1" | awk  '{printf "%5.4e\n",$0}'


 

return 0;

#PowerToExp_end_
}

ExpToPower()
{

#ExpToPower_begin_
local help='

make transformation of the form from 1.2e-2 to 0.012
#$1 -- input number
'
        if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi


local res="$1";

local ind=`expr index "$1" "e"`;
if [ $ind -gt 0 ]
then

local mantisa=`echo "$1" |  sed -e 's/\(.*\)e\(.*\)/\1/'`
 res="$mantisa";
local exponenta=`echo "$1" |  sed -e 's/\(.*\)e\(.*\)/\2/'`

exponenta=`echo $exponenta | sed -e 's/+0*\(.*\)/\1/'` 

if [ -z $exponenta ]; then exponenta=0; fi;

if [ $exponenta -gt 0 ]
then
        for (( i = 0; i < $exponenta; i++ ))
        do
                res=`echo "scale=5; $res * 10" | bc `;
        done
else
         for (( i = 0; i > $exponenta; i-- ))
        do
                res=`echo "scale=20; $res / 10" | bc `;
        done
fi
fi
echo $res
return;

#ExpToPower_end_
}

	
Change_word_comment_line()
{
#Change_word_comment_line_begin_
local help='
change the word in line; comment old line; insert a new line with changed "word"

Change_word_comment_line:

#$1 needed to find line
#$2 needed to find line
#$3 word to replace by
#$4 new word
#$5 comment symbol
#$6 the file to change
#$7 the new file
'


if [ $# -lt 7 ]; then print_help "$0" "$help"; return 1; fi

#if $4 contains '/'
local newString=`echo  $4 | sed 's%/%\\\\/%g'`


	 change_line=`findLine "$1" "$2" "$6"`;
         numline=`findLineNumber "$1" "$2" "$6" `
	where_to_insert=`echo "scale=0; $numline+1"|bc`


#echo $change_line
#echo $numline
#echo $where_to_insert

#continue;

	if [ -n "${change_line}" ]
	then



		change_newline1=`echo "${change_line}" | sed -e "s/$3/$newString/"`;
		change_newline2=`echo "$5${change_line}"`;

#			echo $change_newline1
#			echo $change_newline2
		
		fl_tmp=tmp_$$;
		fl_tmp2=tmp2_$$;

		sed -e "
		${numline}{
		c\
		 $change_newline2\n
		}" $6 >  ${fl_tmp}; 
#		mv  ${fl_tmp}  $7;


		sed -e "
                ${where_to_insert}{
                c\
                 $change_newline1
                }"   ${fl_tmp} >  ${fl_tmp2}; 
                mv  ${fl_tmp2}  $7;


	rm ${fl_tmp};

	fi

#Change_word_comment_line_end_
}

Make_Table()
{
#Make_Table_begin_
local help='
#$1 header of table
#
#$2 file containing table in groff(tbl) format:
#
#	aa ;	bb ;	cc
#	dd ;	vv ;	etc
#
#
#
#$3 -- type: ps or ascii

Example of use 

lets assume that \$2 is "test.tbl", then its easy to get pdf version of the table, doing

Make_Table  "Test of the table" test.tbl ascii > test.rst; rst2pdf test.rst 
Make_Table  "Test of the table" test.tbl ascii  | sed -e "4,6s/-/=/g" | awk "{print \"           \",$0}"  > test.rst
Make_Table  "Test of the table" HLT.tbl  ascii |  sed -e "4,6s/-/=/g" -e "s/+/ /g" -e "s/|//g" -e "/-/d" | awk {print "           ",$0}
'

if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi


#Define format! --> Maximal number of fields!

#Initial max of NF:
maxNF=`cat $2 | sed -n "1p" | awk -F';' "{print NF}"`;
nl=`cat $2| wc -l`;


for (( i=1; i<=$nl;i++ ))
do

curLine=`cat $2 | sed -ne "${i}p"`;


curNF=`echo $curLine |  awk -F';' '{print NF}'`;
cmpr=`echo "$maxNF > $curNF" | bc`;

if [ $cmpr -eq 0 ] ; then maxNF=$curNF; fi 

done



echo '.TS
allbox tab(;);' > tmp.file;
for (( i=1; i<=$maxNF;i++ ))
do
echo -n "c" >> tmp.file;
done
echo  '.' >> tmp.file;
cat $2 >> tmp.file;
echo '.TE' >> tmp.file;


	echo "		$1	";
    echo "  "
    echo "  "

# tbl table.tbl | troff -Tascii | grotty 2>|/dev/null  | sed -e '/^$/d'
if [  "$3" = 'ascii' ];
then
tbl tmp.file | troff -Tascii |  grotty 2>|/dev/null  | sed -e '/^$/d';
fi;

if [  "$3" = 'ps' ];
then
tbl tmp.file | troff -Tps |  grops 2>|/dev/null > $2.ps;
fi;
rm tmp.file;
return 0;
#Make_Table_end_
}

Find_vars_to_replace()
{
#Find_vars_to_replace_begin_
local help='
#$1 vars definitions: like %%anyVar%% -->%%
#$2 files to process: string type!!
'


if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

#find the number of input files:
len=`echo $2 | tr " " ":"  | awk -F":" ' END{ print NF}'`;


	echo ">>   `pwd`";

textFileNames=();
textSettings=();
textCfg=();


for (( i=1;i<=$len;i++ ))
do
	curFileName=`echo $2  | awk  "{print \\$${i}}" `;
	lineNumber=`sed -ne "/.*$1.*$1.*/=" $curFileName`;

		echo ">>   $curFileName   >> contains >> ";
		echo ;
		echo ;


	for k in $lineNumber
	do

	curLine=`sed -ne "${k}p" $curFileName`;
#	echo curLine $curLine 

	if [ -n "$curLine" ] 
	then 

	textFileNames=($(echo $textFileNames | tr ' ' '\n') $curFileName);

	tempVars=`echo $curLine | sed -n  -e ':a' -e  "s/\(.*\)$1\(.*\)$1\(.*\)/\2;;;\1/p;ta"  >| tmp.file; sed -ne "\`cat tmp.file | wc -l\`p" tmp.file; rm tmp.file`;
#	echo "tempvar1" $tempVars;
	tempVars=`echo $tempVars | sed -ne 's/\(.*\)\(;;;.*$\)/\1/p' | tr ';;;' ' '`;
#	echo "tempvar2"  $tempVars;

		vars=($( echo "$tempVars" | tr ' ' '\n'));
		

#		echo "${#vars[*]}"
#		echo "vars3" ${vars[0]};
#		echo "vars3" ${vars[1]};
#		echo "vars3" ${vars[2]};
#		echo "vars3" ${vars[3]};
#
#continue;

		for ((j=0;j<${#vars[@]};j++))
		do

		echo "export ${vars[$j]}=?  		# >> code for 'settings.cfg' >> "
	

		done


		for ((j=0;j<${#vars[@]};j++))
		do

		echo "Changeword '$1${vars[$j]}$1' \$${vars[$j]} $curFileName $curFileName;     # >> code for 'ini.cfg' >> "


		done

	
	fi



	done	

	

done

return 0;
#Find_vars_to_replace_end_
}

Changefield()
{
#Changefield_begin_
local help='
#$1 needed to find line
#$2 needed to find line
#$3 number of field to replace
#$4 new value
#$5 the file to change
#$6 the new file 
'


if [ $# -lt 6 ]; then print_help "$0" "$help"; return 1; fi


	
	 change_line=`findLine "$1" "$2" "$5"`;
	 numline=`findLineNumber "$1" "$2" "$5" `


	if [ -n "${change_line}" ]
	then

		 change_field=`echo "${change_line}" | awk  "{print \\$$3 }"`; 
		 change_newline=`echo "${change_line}" | sed -e "s/${change_field}/$4/"`;
		fl_tmp=tmp_$$;

		sed -e "
		${numline}{
		c\
		 $change_newline
		}" $5 >  ${fl_tmp}; 
		mv  ${fl_tmp}  $6;


	fi
#Changefield_end_	
}


Changeword()
{
#Changeword_begin_
local help='
#$1 old word to be replaced by
#$2 new word
#$3 old file
#$4 new file
'


if [ $# -lt 4 ]; then print_help "$0" "$help"; return 1; fi



local tmpfile=$$_tmp_1;

#if $1 contains '/'
local newString=`echo  "$2" | sed 's%/%\\\\/%g'` 

#echo $newString

cat $3 | sed "s/$1/$newString/" > $tmpfile; 


mv $tmpfile $4;
#Changeword_end_
}

Random_number()
{
#Random_number_begin_
local ISEED=`dd if=/dev/urandom count=1 2> /dev/null | cksum | cut -b-5`
echo $ISEED
#Random_number_end_
}

Combine_content()
{
#Combine_content_begin_
local help='
###produces the list :  file1,
##			file2,
##			file3	
##			etc
#$1 input file
'


if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi


local total_lines=`cat $1 | wc -l`
local content="";
for ((i=1;i<=$total_lines;++i))
do	

	tmp_read_line=`sed -ne ${i}p  $1`


	if [ -n "${tmp_read_line}" -a  $i -lt $total_lines ]
		then
			content="${content} \n
				 \"${tmp_read_line}\","
		fi

	if [ -n "${tmp_read_line}" -a  $i -eq $total_lines ]
		then
			content="${content} \n
				 \"${tmp_read_line}\""
		fi

done
		
	
		echo -e $content
#Combine_content_end_
}




Insert_here()
{
#Insert_here_begin_
local help='
Insert_here:

#$1 -- file to be modified
#$2 -- where to insert
#$3 -- file to be inserted
'


if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

	if [ -n "$2" -a -n "$1" -a -n "$3" ]
	then 
		local tmp_file="$$_file_tmp";
		local tmp_file2="$$_file_tmp2";
		cp $1 $tmp_file;
		sed -e "/$2/r $3" < $tmp_file > $tmp_file2;
		rm $tmp_file;
		mv $tmp_file2 $1
	else 
		
		echo "Insert_here():: Something is wrong"
	fi
	
#Insert_here_end_
}

FixAlpgenLHE()
{
#FixAlpgenLHE_begin_

local help='
#$1 -- file to be modified
#$2 -- from to be removed
#$3 -- until to be removed
#$4 -- mode: 0 remove lines with keywords $2  and $3
#$4 -- mode: 1 leave them unchanged
'

if [ $# -lt 4 ]; then print_help "$0" "$help"; return 1; fi

if [ -n "$2" -a -n "$1" -a -n "$3" -a -n "$4" ]
        then
                local tmp_file="$$_file_tmp";
                local tmp_file2="$$_file_tmp2";
                cp $1 $tmp_file;
###Print only first first field of the current string match to \$2
if [ $4 -eq 0 ]
then
awk "BEGIN{act=0} { if (match(\$0,\"$3\")) act=0; if (act==0 && !match(\$0,\"$2\") && !match(\$0,\"$3\") )  print \$0; if (act>0) { }; if (match(\$0,\"$2\")){ act=1}; }" $tmp_file > $tmp_file2;
else
awk "BEGIN{act=0} { if (match(\$0,\"$3\")) {act=0; print \$0};  act=0; if (act==0 && !match(\$0,\"$2\") && !match(\$0,\"$3\") )  print \$0; if (act>0) { }; if (match(\$0,\"$2\")){ act=1; print \$0}; }" $tmp_file > $tmp_file2;
fi
                mv $tmp_file2 $1;
                rm $tmp_file;
fi

#FixAlpgenLHE_end_
}


ValidateAlpgenLHE()
{
#ValidateAlpgenLHE_begin_
local help='
#$1 -- file to be validated
#$2 -- from to be validated
#$3 -- until to be validated
'

if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

if [ -n $2 -a -n $1 -a -n $3 ]
        then
                local tmp_file="$$_file_tmp";
                local tmp_file2="$$_file_tmp2";
                cp $1 $tmp_file;
###Print only first first field of the current string match to \$2
awk "BEGIN{act=0} { if (match(\$0,\"$3\")) {act=0; print \$0 ; }; if (act==0 ) { }; if (act>0) { prin \$0 }; if (match(\$0,\"$2\")){ act=1 ; print \$0}; }" $tmp_file > $tmp_file2;
	
                cat $tmp_file2 
                rm $tmp_file2;
                rm $tmp_file;
fi

#ValidateAlpgenLHE_end_
}

TestFunction()
{
#TestFunction_begin_

local help='
#$1 -- file to be processed
#$2 -- name of function to be tested

return 0 if all is ok.
return 1 if something is bad.
'
if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

rm  $$_fsdsadf_tmp >& /dev/null;
ExtractFunctionBody  $1 $2 > $$_fsdsadf_tmp;

#cat $$_fsdsadf_tmp; ;
source $$_fsdsadf_tmp;
echo "Try to get help, running the command without parameters"
($2) | grep "usage" >& /dev/null;echo $?
echo "Try to get help, running the command with help parameter"
($2 "help") | grep "usage" >& /dev/null;echo $?  
rm  $$_fsdsadf_tmp;

#TestFunction_end_
}

HLT_TREE()
{
#HLT_TREE_begin_


local help=' 

it ceates HLT structure:

Example
we want to view the structure of all sequences in the path  
HLT_Jet60Eta1p7_Jet53Eta1p7_DiBTagIP3DFastPV_v1

where sequences matching patterns "hltL1sL1.*" "HLTBeg.*" "hltPre.*" "HLTDo.*" "HLTEnd.*" "HLT.*Corr.*" are excluded 

HLT_TREE HLT_GRun_cff.py HLT_Jet60Eta1p7_Jet53Eta1p7_DiBTagIP3DFastPV_v1 " "  "hltL1sL1.*" "HLTBeg.*" "hltPre.*" "HLTDo.*" "HLTEnd.*" "HLT.*Corr.*"


Notation: 

1) cms.Path
+++ Name +++ 

2) cms.Sequence
--- Name ---

3) cms.EDFilter or cms.Producer module`
*** Name ***

4) input tags of a module
~~~ Name ~~~
'

if [ $# -lt 3  ]; then print_help "$0" "$help"; return 1; fi
if [ "$1" = help ];  then print_help "$0" "$help"; return 1; fi

local irrelevant_modules;

local a1;
local a2;
local a3;

local args;
args=("$@");
local add;
for ((i=4;i<=$#;i++))
do
        add=${args[$i]};
        irrelevant_modules=`echo $irrelevant_modules $add`;

done

a1=($( HLT_PATH $1 $2 $irrelevant_modules ));
a2=($( HLT_SEQUENCE $1 $2 $irrelevant_modules ));
a3=($( HLT_MODULE $1 $2 $irrelevant_modules ))

#echo "a1 = $a1"
#echo "a2 = $a2"
#echo "a3 = $a3"



local b1;
local b2;
local c;
b1=0;
b2=0;

if [ -n "$a1" ]; then echo "$3 +++ $2 +++";b1=1;fi;
if [ -n "$a2" ]; then echo "$3 --- $2 ---";b2=1;fi;
if [ -z "$a1" -a -z "$a2" ]; then echo "$3 *** $2 ***";fi;

if [ -n "$a3" ]
then 
	for j in "${a3[@]}"
	do
		echo "$3	~~~ $j ~~~";
	done
fi
#for (( i=1;i<=${#a[@]};i++ ))
if [ $b1 -gt 0 ]
then
	for j in "${a1[@]}"
	do
#	echo HLTPATH $j
#	bbb=($( HLT_SEQUENCE $1 $j  $irrelevant_modules ));
	HLT_TREE $1 $j  "$3	" $irrelevant_modules
	done
fi


if [ $b2 -gt 0 ]
then
	for j in "${a2[@]}"
	do
#	echo HLTSEQ $j
#	bbb=($( HLT_SEQUENCE $1 $j  $irrelevant_modules ));
	HLT_TREE $1 $j "$3	" $irrelevant_modules
	done
fi


#HLT_TREE_end_
}

HLT_PATH()
{
#HLT_PATH_begin_	
local help='

That is the part of processing HLT_MENU.cff file

#$1 -- HLT MENU file
#$2 -- HLT path , exact name
#$3 -- what sequences and modules to ignore

return the list of modules and HLT sequences
'

if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

local a;
#a=($(grep $2 $1 | grep "cms.Path" | tr ' ' '\n'))
a=($(grep -w -E "^$2"  $1 | grep "cms.Path" ))
#echo "$a"

local irrelevant_modules;
irrelevant_modules="$2";

local args;
args=("$@");
local add;
for ((i=3;i<=$#;i++))
do
        add=${args[$i]};
        irrelevant_modules=`echo $irrelevant_modules $add`; 

done

 
#echo 1212232 $irrelevant_modules
#echo 1212232 
b=$(Remove_parts_string "$a" "cms.Path" "(" ")"  "+" "=" $irrelevant_modules | tr ' ' '\n')
echo "$b"

#HLT_PATH_end_
}

HLT_SEQUENCE()
{
#HLT_SEQUENCE_begin_	
local help='

That is the part of processing HLT_MENU.cff file

#$1 -- HLT MENU file
#$2 -- HLT SEQUENCE , exact name
#$3 -- what sequences and modules to ignore
return the list of modules in HLT sequences

Example 
 HLT_SEQUENCE HLT_GRun_cff.py  HLTBTagIPSequenceL25bbPhiL1FastJet "HLTDoLocalPixelSequence hltSelector4JetsL1FastJet"

'

if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

local a;
#a=($(grep $2 $1 | grep "cms.Path" | tr ' ' '\n'))
a=($(grep -w -E "^$2" $1 | grep "cms.Sequence" ))
#a=`grep -w $2 $1 | grep "cms.Sequence" `

#echo $a
#echo 
#echo 
#echo

local b;

local irrelevant_modules;
irrelevant_modules="$2";

local args;
args=("$@");
local add;
for ((i=3;i<=$#;i++))
do
        add=${args[$i]};
        irrelevant_modules=`echo $irrelevant_modules $add`;

done


#Remove_parts_string "$a" "cms.Sequence" "(" ")" "+" "="  $irrelevant_modules 
b=$(Remove_parts_string "$a" "cms.Sequence" "(" ")" "+" "="  $irrelevant_modules | tr ' ' '\n')
echo "$b"


#HLT_SEQUENCE_end_
}


HLT_MODULE()
{
#HLT_MODULE_begin_
local help='

That is the part of processing HLT_MENU.cff file

#$1 -- HLT MENU file
#$2 -- HLT MODULE , exact name
#$3 -- what input tags to ignore
return the list of input tags  of the modules 

Example
	no example
'

if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi
local a;


a=($(Extract_some_part $1 $2 "=" ")" 1 | grep -i "inputtag" | awk '{print $4}' | tr ' ' '\n'))
#a=`grep -w $2 $1 | grep "cms.Sequence" `

#echo "$a"


local irrelevant_modules;

local args;
args=("$@");
local add;
for ((i=3;i<=$#;i++))
do
        add=${args[$i]};
        irrelevant_modules=`echo $irrelevant_modules $add`;

done

local b;

#b=$(Remove_parts_string "$a" "cms.InputTag" "(" ")" "," "="  $irrelevant_modules | tr -d " \" " |tr ' ' '\n')
#echo "$b"


#b=$(Remove_parts_string "$a"  "\"" "'"   $irrelevant_modules  | tr ',' ' ' |tr ' ' '\n'  );
local aaaa;
aaaa=`echo "$a" | tr -d "\"" | tr -d "'"`;
b=$( Remove_parts_string "$aaaa" $irrelevant_modules  | tr ',' ' ' |tr ' ' '\n'  );
echo "$b" | uniq;



#HLT_MODULE_end_
}

Remove_parts_string()
{

#Remove_parts_string_begin_	
local help='
#$1 -- string
#$2 -- word1 
#$3 -- word2
etc to remove 
'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

local args;
args=("$@");
local remove;
local string;

string="$1";

#echo "string = $string";
local aaa;
aaa=($( echo $string));
local bbb;
#echo "aaa=${#aaa[@]}";

local outstring
outstring="";


for ((j=1; j<=${#aaa[@]}; j++))
do

for ((i=2;i<=$#;i++))
do
	remove=${args[$i]};
#	echo remove=$remove
#	echo index=$j
#	bbb=`expr index "$j" "$remove"`;	
	bbb=`echo "${aaa[$j]}" | grep  "$remove"`;	 
#	echo bbb=$bbb
#	echo remove=$remove
	
	if [ -n "$bbb"  ];  then aaa[$j]="";fi;

#	echo j=${aaa[$j]}

	#string=`echo $string | sed -ne "s/$remove//pg"`
#	string=`echo ${string//"$remove"/}`

#	echo  "${args[$i]}" 
#	echo strin=$string
	done 
done

for ((j=1; j<=${#aaa[@]}; j++))
do
	outstring=`echo $outstring ${aaa[$j]}`;

done
#echo $string
echo $outstring

#Remove_parts_string_end_
}


Extract_some_part()
{
#Extract_some_part_begin_	
local help='
#$1 -- file to be processed
#$2 -- word1 in the begin line
#$3 -- word2 in the begin line
	position of word1 must be < postion of word2
#$4 -- word in the  end line
#$5 -- position of the word in the end line

That is the part of processing HLT_MENU.cff file

'

if [ $# -lt 5 ]; then print_help "$0" "$help"; return 1; fi

#beg=`findLineNumber "$2" "$3" "$1"`
#end=`findLineNumber "$4" "$5" "$1"`
#echo $beg
#echo $end
#sed -ne "$beg,${end}p" $1 >  $$_file_tmp;

#awk 'BEGIN{ found=0 };  { if (index($0,"hltBLifetimeL3BJetTagsbbPhiL1FastJetFastPV")==1 && match($0,"=")>0) {found=1}; if(found>0) print $0; if (index($0,")")==1)  found=0;} END{ print found}' HLT_GRun_cff.py



awk "BEGIN{ found=0 };  { if (index(\$0,\"$2\")>0 &&  index(\$0,\"$3\")>0 && index(\$0,\"$2\")< index(\$0,\"$3\")) {found=1}; if(found>0) print \$0; if (index(\$0,\"$4\")==$5)  found=0;}" $1



#cat    $$_file_tmp;
#rm $$_file_tmp;

#Extract_some_part_end_
}

ExtractFunctionBody()
{
#ExtractFunctionBody_begin_

local help='
#$1 -- file to be processed
#$2 -- name of function
'

if [ $# -lt 2 ]; then print_help "$0" "$help"; return 1; fi

local beg=`findLineNumber "#$2_begin_" "" "$1" `
local end=`findLineNumber "#$2_end_" "" "$1"` 
sed -ne "$beg,${end}p" $1 >  $$_file_tmp;
echo "$2()";
echo "{";
cat    $$_file_tmp;
echo "}";

rm $$_file_tmp;


#ExtractFunctionBody_end_
}


Comment()
{

#Comment_begin_

local help='
#$1 -- file to be modified
#$2 -- from to be commented
#$3 -- until to be commented
#$4 --symbol of comments (optional)
'

if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

if [ -n "$2" -a -n "$1" -a -n "$3" ]
	then
		local tmp_file="$$_file_tmp";
		local tmp_file2="$$_file_tmp2";
		cp $1 $tmp_file;

		if [ -z $4 ]
		 then
		 awk "BEGIN{act=0} { if (match(\$0,\"$3\")) act=0; if (act==0) print \$0; if (act>0) { print \"#\"\$0 }; if (match(\$0,\"$2\")) act=1;    }" $tmp_file > $tmp_file2;
		else
		 awk "BEGIN{act=0} { if (match(\$0,\"$3\")) act=0; if (act==0) print \$0; if (act>0) { print \"$4\"\$0 }; if (match(\$0,\"$2\")) act=1;    }" $tmp_file > $tmp_file2;
		fi		
		mv $tmp_file2 $1;
		rm $tmp_file;
fi

#Comment_end_ 
}

UnComment()
{

#UnComment_begin_

local help='
#$1 -- file to be modified
#$2 -- from to be uncommented
#$3 -- until to be uncommented
#$4 --symbol of comments (optional)
'


if [ $# -lt 3 ]; then print_help "$0" "$help"; return 1; fi

if [ -n $2 -a -n $1 -a -n $3 ]
	then
	local tmp_file="$$_file_tmp";
	 local tmp_file2="$$_file_tmp2";
	cp $1 $tmp_file;
	 if [ -z $4 ]
                 then
	awk "BEGIN{act=0} {if (match(\$0,\"$3\")) act=0;if (act==0) print \$0;  if (act>0) { str=\$0; sub(\"#\",\"\",str); print str}; if (match(\$0,\"$2\")) act=1;}" $tmp_file > $tmp_file2;
	else
	awk "BEGIN{act=0} {if (match(\$0,\"$3\")) act=0;if (act==0) print \$0;  if (act>0) { str=\$0; sub(\"$4\",\"\",str); print str}; if (match(\$0,\"$2\")) act=1;}" $tmp_file > $tmp_file2;

	fi	
		 mv $tmp_file2 $1;
                rm $tmp_file;
fi

#UnComment_end_
}


UnCommentConcrete()
{
#UnCommentConcrete_begin_ 
local help='
#$1 -- file to be modified
#$2 -- from to be uncommented
#$3 -- until to be uncommented
#$4 -- what to uncomment
#$5 -- what to be not presented at the string
'


if [ $# -lt 5 ]; then print_help "$0" "$help"; return 1; fi


	local tmp_file="$$_file_tmp";
 	local tmp_file2="$$_file_tmp2";
        cp $1 $tmp_file;

if [ -z "$5" ]
	then
	awk "BEGIN{act=0} {if (match(\$0,\"$3\")) act=0;if (act==0) print \$0;  if (act>0 && match(\$0,\"$4\")) { str=\$0; sub(\"#\",\"\",str); print str}; if (match(\$0,\"$2\")) act=1;}" $tmp_file > $tmp_file2;
fi

if [ -n "$5" ] 
	then
	awk "BEGIN{act=0} {if (match(\$0,\"$3\")) act=0;if (act==0) print \$0;  
	if (act>0){ if (match(\$0,\"$4\") && !match(\$0,\"$5\")){
	  str=\$0; sub(\"#\",\"\",str); print str}; if (!(match(\$0,\"$4\") && !match(\$0,\"$5\")))  print  \$0 }; if (match(\$0,\"$2\")) act=1;}" $tmp_file >  $tmp_file2;
fi

		 mv $tmp_file2 $1;
                rm $tmp_file;

#UnCommentConcrete_end_ 
}

Test_all_functions()
{
#Test_all_functions_begin_
local help='
test all functions in the file 

#$1
'

if [ $# -lt 1 ]; then print_help "$0" "$help"; return 1; fi

a=`print_all_functions $1`;
a=($(echo $a | tr ' ' '\n'));

for ((i=1;i<=${#a[@]};i++))
do
_file=${RANDOM}_test_tmp_;
ExtractFunctionBody  $1 ${a[$i]} > $_file
echo  Testing : ${a[$i]}
source  $_file ;
[ "$?" != 0 ] &&  echo "problems with ${a[$i]}"

done

#Test_all_functions_end_
}


